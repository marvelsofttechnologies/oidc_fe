import React, { Component } from 'react';
import {MainContext} from '../contexts/MainContext';

class Modal extends Component {

    componentDidMount() {
        if(this.props.unloadCallback){
          setTimeout(() => {
            eval(`$('#`+this.props.id+`').on('hidden.bs.modal', ()=> {this.props.unloadCallback();})`);  
          }, 3000);
        }
      }
      
    render() {
        return (  
            <div className="modal" id={this.props.id} tabIndex="-1"
        role="dialog" aria-labelledby={"modal-" + this.props.id} aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id={"modal-" + this.props.id}>
                {this.props.title}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body  pt-5 pb-5 pr-5 pl-5">
            {this.props.children}
            </div>
          </div>
        </div>
      </div>
    
        );
    }
}

export default Modal;