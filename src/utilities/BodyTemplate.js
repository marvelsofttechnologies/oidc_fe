import React, { Component } from "react";
import {MainContext} from '../contexts/MainContext';
import ErrorBoard from "./ErrorBoard";
import Spinner from './Spinner';

class BodyTemplate extends Component {
    state ={
        loading:false
    }
  render() {
    return (
      this.props.showErrorBoard ? <ErrorBoard /> :
      <>
        <MainContext.Consumer>
          
          {/* <SideNav/> */}
          {(context) => (
            <div
              className={
                context.navOpen ? "body-content expand" : "body-content"
              }
            >
                {this.props.loading ?
                <>
                <div className="text-center">
                  <Spinner loading={this.state.loading} />
                </div>
              </>
            :
            <div className="container-fluid">
              {this.props.children}
            </div>
            
            }
             </div> 
          )}
        </MainContext.Consumer>
      </>
    );
  }
}

export default BodyTemplate;
