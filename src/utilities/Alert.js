import React, { Component } from "react";
import { MainContext } from "../contexts/MainContext";

class Alert extends Component {
  render() {
    return (
      <MainContext.Consumer>
        {(context) => (
          <>
            {context.alert.show ? (
              <div className={`alert alert-${context.alert.type}`} role="alert">
                <h4 className="alert-heading">{context.alert.title}</h4>
                <p>{context.alert.message}</p>
              </div>
            ) : null}
          </>
        )}
      </MainContext.Consumer>
    );
  }
}

export default Alert;
