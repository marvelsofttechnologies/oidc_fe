import React, { Component } from "react";
import { MainContext } from "../contexts/MainContext";
import {Link} from 'react-router-dom';

class ErrorBoard extends Component {
  reload = () => {
    window.location.reload();
  };
  render() {
    return (
      <>
        <MainContext.Consumer>
          {/* <SideNav/> */}
          {(context) => (
            <div
              className={
                context.navOpen ? "body-content expand" : "body-content"
              }
            >
              <div className="container-fluid">
                <div className="error-body">
                  <div className="error-content">
                    <div className="first-plug">
                      <svg
                        width={703}
                        height={90}
                        viewBox="0 0 703 90"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect y={39} width={625} height={11} fill="black" />
                        <path
                          d="M672.037 34.4543L693.589 34.3693C696.137 34.3718 698.295 33.4199 700.063 31.5135C701.83 29.6072 702.713 27.3113 702.71 24.6259C702.708 21.9405 701.821 19.6429 700.049 17.7331C698.278 15.8233 696.118 14.8672 693.57 14.8647L672.018 14.9497L672.003 0.109209L668.712 0C658.024 0.0602483 648.561 3.42545 640.321 10.0956C632.082 16.7658 626.941 25.1527 624.898 35.2563C624.216 39.4442 623.265 49.2082 624.917 54.7609C627.051 64.798 632.243 73.1596 640.496 79.8459C648.748 86.5321 658.183 89.8804 668.8 89.8908L672.091 90L672.077 75.1595L693.629 75.0745C696.177 75.077 698.335 74.1251 700.102 72.2188C701.87 70.3124 702.753 68.0166 702.75 65.3312C702.747 62.6457 701.86 60.3481 700.089 58.4383C698.318 56.5286 696.158 55.5724 693.61 55.57L672.058 55.6549L672.037 34.4543Z"
                          fill="black"
                        />
                      </svg>
                    </div>
                    <h4 className="error-text">An Error Occured </h4>
                    <div className="second-plug">
                      <svg
                        width={668}
                        height={90}
                        viewBox="0 0 668 90"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          x={43}
                          y={39}
                          width={625}
                          height={11}
                          fill="black"
                        />
                        <path
                          d="M31.4147 79.9867C39.5846 73.3008 44.6822 64.894 46.7077 54.7665C48.0501 47.2497 47.3247 38.6717 46.7942 35.3222C44.6087 25.1906 39.4246 16.7738 31.2418 10.0717C23.059 3.36969 13.7039 0.0134884 3.17643 0.0031121L0.0181879 0L0 89.9967L3.36878 90C13.8962 90.0103 23.2449 86.6726 31.4147 79.9867Z"
                          fill="black"
                        />
                      </svg>
                    </div>
                    <Link onClick={this.reload}>
                    <i className="fas fa-redo" onClick={this.reload} />
                    <p onClick={this.reload}>try again</p>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          )}
        </MainContext.Consumer>
      </>
    );
  }
}

export default ErrorBoard;
