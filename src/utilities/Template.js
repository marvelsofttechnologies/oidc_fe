import React, { Component } from "react";
import SideNav from "../components/Navigations/SideNav";
import TopNav from "../components/Navigations/TopNav";

class Template extends Component {
  render() {
    return (
      <>
        {this.props.isLoginPath ? (
          <>
            <div className="d-flex w-100">
              <SideNav location={this.props.location} />
              <div className="w-100">
                <TopNav expanded={this.props.expanded} />
                {this.props.children}
              </div>
            </div>
          </>
        ) : (
          <>{this.props.children}</>
        )}
      </>
    );
  }
}

export default Template;
