import React from "react";
import { Route, Switch } from "react-router-dom";
import SignIn from "./pages/SIgnIn";
import Dashboard from "./components/DashBoard/Index";
import ForgotPassword from "./pages/PasswordRecovery/ForgotPassword";
import CompleteRecovery from "./pages/PasswordRecovery/CompleteRecovery";
import Subscribers from "./pages/Subscriber/Subscribers";
import SideNav from "./components/Navigations/SideNav";
import Vendors from "./pages/Vendor/Vendors";
import AddVendor from "./pages/Vendor/AddVendor";
import Proposals from "./pages/Vendor/Proposals";
import EditVendor from "./pages/Vendor/EditVendor";
import PlotInventory from "./pages/PlotInventory/Index";
import AddPlot from "./pages/PlotInventory/AddPlot";
import EditPlot from "./pages/PlotInventory/EditPlot";
import AllPlots from "./pages/PlotInventory/AllPlots";
import PlotWithInterest from "./pages/PlotInventory/PlotWithInterest";
import AddSubscriber from "./pages/Subscriber/AddSubscriber";
import Calender from "./pages/Calender/Calender";
import Payments from "./pages/Payments/Payments";
import Requests from "./pages/Requests/Requests";
import SubscriberDetails from "./pages/Subscriber/SubscriberDetails";
import SingleRequest from "./pages/Requests/SingleRequest";
import SinglePlot from "./pages/PlotInventory/SinglePlot";
import EditSubscriber from "./pages/Subscriber/EditSubscriber";
import Communication from "./pages/Communication/Communication";
import CreateOffer from "./pages/Offers/CreateOffer";
import Offers from "./pages/Offers/Offers";
import VendorDetail from "./pages/Vendor/VendorDetail";
import AddJobCard from "./pages/Vendor/AddJobCard";

const ROUTES = [
  { path: "/", key: "ROOT", exact: true, component: () => <SignIn /> },
  {
    path: "/login",
    key: "AUTH",
    component: RenderRoutes,
    routes: [
      {
        path: "/login",
        key: "AUTH_ROOT",
        exact: true,
        component: () => <SignIn />,
      },
      {
        path: "/login/reset",
        key: "AUTH_ROOT",
        exact: true,
        component: () => <ForgotPassword />,
      },
      {
        path: "/login/completereset/:code",
        key: "AUTH_ROOT",
        exact: true,
        component: (props) => <CompleteRecovery {...props} />,
      },
    ],
  },
  {
    path: "/calender",
    key: "CALENDER",
    exact: true,
    component: (props) => <Calender {...props} />,
  },
  {
    path: "/subscribers",
    key: "APP_PAGE",
    component: RenderRoutes,
    routes: [
      {
        path: "/subscribers",
        key: "APP_PAGE",
        exact: true,
        component: () => <Subscribers />,
      },
      {
        path: "/subscribers/add",
        key: "APP_PAGE",
        exact: true,
        component: () => <AddSubscriber />,
      },
      {
        path: "/subscribers/:id",
        key: "APP_PAGE",
        exact: true,
        component: (props) => <SubscriberDetails {...props} />,
      },
      {
        path: "/subscribers/edit/:id",
        key: "APP_PAGE",
        exact: true,
        component: (props) => <EditSubscriber {...props} />,
      },
    ],
  },
  {
    path: "/vendors",
    key: "VENDOR",
    component: RenderRoutes,
    routes: [
      {
        path: "/vendors",
        key: "VENDOR_PAGE",
        exact: true,
        component: (props) => <Vendors />,
      },
      {
        path: "/vendors/add",
        key: "VENDOR_PAGE",
        exact: true,
        component: (props) => <AddVendor {...props} />,
      },
      {
        path: "/vendors/jobs/create",
        key: "VENDOR_PAGE",
        exact: true,
        component: (props) => <AddJobCard {...props} />,
      },
      {
        path: "/vendors/edit/:id",
        key: "VENDOR_PAGE",
        exact: true,
        component: (props) => <EditVendor {...props} />,
      },
      {
        path: "/vendors/:id",
        key: "VENDOR_PAGE",
        exact: true,
        component: (props) => <VendorDetail {...props} />,
      },
    ],
  },
  {
    path: "/proposals",
    key: "PROPOSALS",
    component: RenderRoutes,
    routes: [
      {
        path: "/proposals",
        key: "Proposals",
        exact: true,
        component: () => <Proposals />,
      },
    ],
  },
  {
    path: "/plots",
    key: "PLOTS_PAGE",
    component: RenderRoutes,
    routes: [
      {
        path: "/plots",
        key: "PLOTS_PAGE",
        exact: true,
        component: () => <PlotInventory />,
      },
      {
        path: "/plots/newplot",
        key: "PLOTS_PAGE",
        exact: true,
        component: () => <AddPlot />,
      },
      {
        path: "/plots/editplot/:plotId",
        key: "PLOTS_PAGE",
        exact: true,
        component: (props) => <EditPlot {...props} />,
      },
      {
        path: "/plots/all",
        key: "PLOTS_PAGE",
        exact: true,
        component: () => <AllPlots />,
      },
      {
        path: "/plots/interest",
        key: "PLOTS_PAGE",
        exact: true,
        component: () => <PlotWithInterest />,
      },
      {
        path: "/plots/:id",
        key: "PLOTS_PAGE",
        exact: true,
        component: (props) => <SinglePlot {...props} />,
      },
    ],
  },
  {
    path: "/requests",
    key: "APP",
    component: RenderRoutes,
    routes: [
      {
        path: "/requests",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <Requests />,
      },
      {
        path: "/requests/:id",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <SingleRequest {...props} />,
      },
    ],
  },
  {
    path: "/payments",
    key: "APP",
    component: RenderRoutes,
    routes: [
      {
        path: "/payments",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <Payments />,
      },
    ],
  },
  {
    path: "/app",
    key: "APP",
    component: RenderRoutes,
    routes: [
      {
        path: "/app",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <Dashboard {...props} />,
      },
    ],
  },
  {
    path: "/communications",
    key: "APP",
    component: RenderRoutes,
    routes: [
      {
        path: "/communications",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <Communication {...props} />,
      },
    ],
  },
  {
    path: "/offers",
    key: "APP",
    component: RenderRoutes,
    routes: [
      {
        path: "/offers",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <Offers {...props} />,
      },
      {
        path: "/offers/add",
        key: "APP_ROOT",
        exact: true,
        component: (props) => <CreateOffer {...props} />,
      },
    ],
  },
];

export default ROUTES;

function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
}

export function RenderRoutes({ routes }) {
  return (
    <>
      <Switch>
        {routes.map((route, i) => {
          return <RouteWithSubRoutes key={route.key} {...route} />;
        })}
        <Route component={() => <h1>Not Found!</h1>} />
      </Switch>
    </>
  );
}
