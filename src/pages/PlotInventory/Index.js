import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import { Link } from "react-router-dom";
import PlotAllocation from "../../components/Charts/PlotAllocation";
import AquiredPlots from "../../components/Charts/AquiredPlots";
import NewSubscriber from "../../components/Charts/NewSubscriber";
import { HorizontalBar } from "react-chartjs-2";
import PlotDevelopment from "../../components/Charts/PlotDevelopment";
import NumberOfPlots from "../../components/Charts/NumberOfPlots";
import Spinner from "../../utilities/Spinner";
import AddPlot from "../../components/Charts/AddPlot";
import BodyTemplate from "../../utilities/BodyTemplate";

class Index extends Component {
  state = {
    loading: false,
  };
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div className="row">
          <div className="col-lg-2">
            {" "}
            <Link to="/plots/all" className="vend-btn">
              Inventory
            </Link>
          </div>
          <div className="col-lg-2">
            {" "}
            <Link to="/plots" className="vend-btn orange">
              Statistics
            </Link>
          </div>
          {/* <div className="col-lg-2">
            <div className="customs-select plot-inv">
              <select id>
                <option value>Sort</option>
                <option value>Default Payment</option>
                <option value>Default Payment</option>
                <option value>Default Payment</option>
                <option value>Default Payment</option>
              </select>
              <span className="customs-arrow" />
            </div>
          </div>*/}
          <div className="col-lg-2"> 
            {" "}
            <Link to="/plots/newplot" className="vend-btn green">
              Add Plots
            </Link>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-4">
            <div className="plot-box">
              <div className="box-title d-flex w-100 justify-content-between">
                <h5 className="plot-title">Add Plot</h5>
                <span className="boxspan">
                  View details{" "}
                  <img src="./Assets/chevron.png" alt className="ml-2" />
                </span>
              </div>
              <div className="chart-container">
                <AddPlot />
              </div>
              <div className="chart-details float-right mb-5">
                <p className="tot">
                  {" "}
                  <label className="circle tot" />
                  1000 sqm
                </p>
                <span>Unavailable Plots</span>
              </div>
              <div className="chart-details-group d-flex justify-content-between w-100">
                <div className="chart-details">
                  <p>
                    {" "}
                    <label className="circle" />
                    Xxx sqm
                  </p>
                  <span>Unavailable Plots</span>
                </div>
                <div className="chart-details">
                  <p className="wal">
                    {" "}
                    <label className="circle wal" />
                    Xxx sqm
                  </p>
                  <span>Unavailable Plots</span>
                </div>
                <div className="chart-details">
                  <p className="ava">
                    {" "}
                    <label className="circle ava" />
                    Xxx sqm
                  </p>
                  <span>Unavailable Plots</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="plot-box">
              <div className="box-title d-flex w-100 justify-content-between">
                <h5 className="plot-title">Plot Allocation</h5>
              </div>
              <div className="chart-container-2">
                <PlotAllocation />
              </div>
              <h5 className="plot-bottom-text">
                Plot Allocation already in use
              </h5>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="freeplot d-flex align-items-center">
              <div className="freeplotinner">
                <h5 className="plot-title">50</h5>
                <p className="boxspan">Free Plot</p>
              </div>
            </div>
            <div className="plot-chart-box ">
              <div className="week-task">
                <h4 className="aquired">Aquired Plots</h4>
              </div>
              <div className="week-task d-flex justify-content-between">
                <div className="sales">Closed-sales</div>
                <label className="m-n2">
                  <span>
                    Show:
                    <label className="custom">
                      <select id>
                        <option value={0}>Monthly</option>
                        <option value={2}>Yearly</option>
                        <option value={3}>Weekly</option>
                      </select>
                    </label>
                  </span>
                  <span className="pl-2">
                    Show:
                    <label className="custom">
                      <select id>
                        <option value={0}>Online</option>
                        <option value={2}>Offline</option>
                      </select>
                    </label>
                  </span>
                </label>
              </div>
              <div className="divider mt-n2 mb-0" />
              <div className="chart-inner pt-4">
                <AquiredPlots />
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-between"></div>
        <div className="row">
          <div className="col-lg-12">
            <div className="plot-sub">
              <div className="week-task d-flex justify-content-between w-100 px-5">
                <h4 className="aquired">New subscribers by month</h4>
                <div className="sub-chart d-flex">
                  <p className="bl">
                    <label className="circle" />
                    Individual
                  </p>
                  <p className="bl">
                    {" "}
                    <label className="circle com" />
                    Commercial
                  </p>
                </div>
              </div>
              <div className="divider" />
              {/* <div class="chart-inner px-5"> */}
              <div className="plot-chart-inner">
                <NewSubscriber />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="plot-sub">
              <div className="week-task d-flex justify-content-between w-100 px-5">
                <h4 className="aquired">Number of plots sold (sqm)</h4>
                <div className="sub-chart d-flex">
                  <p>
                    <label className="circle" />
                    2019
                  </p>
                  <p>
                    {" "}
                    <label className="circle com" />
                    2020
                  </p>
                </div>
              </div>
              <div className="divider" />
              <div className="plot-chart-inner">
                <NumberOfPlots />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div className="plot-box">
              <div className="box-title d-flex w-100 justify-content-between">
                <h5 className="plot-title">Plot development</h5>
                <span className="boxspan">
                  View details{" "}
                  <img src="./Assets/chevron.png" alt className="ml-2" />
                </span>
              </div>
              <div className="horizontal-bar">
                <PlotDevelopment />
              </div>
            </div>
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default Index;
