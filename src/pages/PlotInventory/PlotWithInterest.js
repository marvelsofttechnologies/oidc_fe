import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from '../../utilities/BodyTemplate';

class PlotWithInterest extends Component {
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div className="row">
          <div className="col-lg-4">
            {" "}
            <button className="vend-btn">ADD PLOT</button>
          </div>
          <div className="col-lg-4">
            {" "}
            <button className="vend-btn ">EDIT PLOT DETAIL</button>
          </div>
          <div className="col-lg-4">
            {" "}
            <button className="vend-btn dark">PLOTS WITH INTEREST</button>
          </div>
        </div>
        <div className="plot-wrap">
          <table className="w-100">
            <thead>
              <tr className="top">
                <th>
                  {" "}
                  <label htmlFor className="top-labels pl-4">
                    Subscriber
                  </label>
                </th>
                <th>
                  {" "}
                  <label htmlFor className="top-labels mr5">
                    Email
                  </label>
                </th>
                <th>
                  {" "}
                  <label htmlFor className="top-labels ">
                    User Id
                  </label>
                </th>
                <th>
                  {" "}
                  <label htmlFor className="top-labels">
                    Phone Number
                  </label>
                </th>
                <th>
                  {" "}
                  <label htmlFor className="top-labels">
                    Plot No
                  </label>
                </th>
                <th>
                  {" "}
                  <label htmlFor className="top-labels mr-0">
                    Date
                  </label>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div className="names-group">
                    <img
                      src="/Assets/Avatar.png"
                      alt
                      className="img-circle lbl"
                    />
                    <label htmlFor="t1" className="names">
                      Lindsey stroud
                    </label>
                  </div>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    lindsey.stroud@gmail.com
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    ABC123
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    08088749339
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    79
                  </label>
                </td>
                <td>
                  <label htmlFor className="names-info">
                    DD/MM/YY
                  </label>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="row">
            <div className="col-lg-4 offset-8">
              <div className="button-box">
                <button className="add-plot-btn">
                  <a href="#">Completed</a>
                </button>
              </div>
            </div>
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default PlotWithInterest;
