import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import Fetch from "../../utilities/Fetch";
import Alert from "../../utilities/Alert";
import BodyTemplate from "../../utilities/BodyTemplate";

class AddPlot extends Component {
  static contextType = MainContext;
  state = {
    sold: false,
    private: true,
    available: false,
    plotNumber: 0,
    size: 0,
    price: 0,
    plotAddress: "",
    open: false,
    tags: "",
    plotTypeId: 0,
    loading: false,
    plotTypes: [],
    selectedFile: null,
    selectedFileName: "",
    base64String:"",
  };

  componentDidMount() {
    this.listPlotTypes();
  }

  onFileChange = (event) => {
    this.setState({
      selectedFile: event.target.files[0],
      selectedFileName: event.target.files[0].name,
    });
    this.getBase64(event.target.files[0],(result) => {
      this.setState({base64String:result});
    });
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  listPlotTypes = () => {
    Fetch("plot/types", "get")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log("okay");
          console.log(data);
          this.setState({ plotTypes: data.data.data }, () => {
            console.log(this.state);
          });
          console.log("okay again");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  addPlot = (e) => {
    const context = this.context;
    let newPlot = {
      plotTypeId: this.state.plotTypeId,
      plotName: this.state.plotName,
      plotAddresss: this.state.plotAddress,
      plotAcres: 0,
      longitude: 0,
      lattitude: 0,
      kilometerSquare: this.state.size,
      amount: this.state.price,
    };
    console.log(newPlot);
    Fetch("plot", "post", newPlot)
      .then((res) => res.json())
      .then((data) => {
        if (data.status) {
          context.showAlert("success", "Plot Added Successfully", "Success");
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
          window.location.href = "/plots/all";
        } else {
          context.showAlert(
            "danger",
            data.message.toString(),
            data.message.toString()
          );
          console.log(data);
          this.setState({ loading: false });
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
        }
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
      });

    e.preventDefault();
  };

  getBase64 =  (file,cb) => {
    console.log(file);
    let reader = new FileReader();
     reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
      //this.setState({base64String:reader.result})
    };

    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }
  
  uploadFile =  (e) => {
    console.log(this.state.base64String)
    
    const formData = new FormData();
    
    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
      );
      
      // Details of the uploaded file
      console.log(this.state.selectedFile);
      e.preventDefault();
    };
    
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <Alert />
        <form onSubmit={this.addPlot}>
          <div>
            <div className="row">
              <div className="col-lg-2">
                <input
                  type="text"
                  id
                  className="signinput-field mb-0"
                  placeholder="Plot Number"
                  name="plotNumber"
                  onChange={this.handleChanges}
                />
              </div>
              <div className="col-lg-2">
                {/* <input type="text" id="" class="signinput-field mb-0" placeholder="Plot Number"> */}
                <div className="select-plot" onChange={this.handleChanges}>
                  <select id="plot" name="plotTypeId">
                    {this.state.plotTypes.map((type, index) => {
                      return (
                        <option value={type.plotTypeId}>
                          {" "}
                          {type.plotTypeName}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div className="col-lg-6">
                <input
                  type="text"
                  id
                  className="signinput-field mb-0"
                  placeholder="Plot Address/Location"
                  name="plotAddress"
                  onChange={this.handleChanges}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5">
                <input
                  type="text"
                  id
                  className="signinput-field mb-1"
                  placeholder="Plot Name"
                  name="plotName"
                  onChange={this.handleChanges}
                />
                {/* <div className="tags">
                          <span>Seperate tags with comma</span>
                          <ul className="tags-li">
                          <li>Residential</li>
                          <li>Available</li>
                          </ul>
                        </div> */}
                <div className="row mt-5">
                  <div className="col-lg-6">
                    <h4 className="plots">Square meter</h4>
                    <input
                      type="text"
                      id
                      className="signinput-field mb-0"
                      placeholder="000 sqm"
                      name="size"
                      onChange={this.handleChanges}
                    />
                  </div>
                  <div className="col-lg-6">
                    <h4 className="plots">Price</h4>
                    <input
                      type="text"
                      id
                      className="signinput-field mb-0"
                      placeholder="000,000,000"
                      name="price"
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-lg-12">
                    <h4 className="plots mb-4">Availability</h4>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="pri"
                        defaultChecked
                        name="private"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="pri">Private</label>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="opn"
                        name="open"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="opn">Open for sale</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="ava"
                        name="available"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="ava">Available for bid</label>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="sol"
                        name="sold"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="sol">Sold</label>
                    </div>
                  </div>
                </div>
                <div className="row mt-5 mb-5">
                  <div className="col-lg-5">
                    <button className="add-plot-btnn" type="submit">
                      Add
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-lg-5">
                <div className="row mt-3">
                  <div className="col-lg-12">
                    <h4 className="plots mb-4">Upload Documents</h4>
                    {this.state.selectedFileName ? (
                      <div className={`alert alert-success`} role="alert">
                        <h4 className="alert-heading">The selected file is:</h4>
                        <p>{this.state.selectedFileName}</p>
                      </div>
                    ) : (
                      <div className={`alert alert-danger`} role="alert">
                        <h4 className="alert-heading">Attention</h4>
                        <p>
                          Please select a file before clicking the upload button
                        </p>
                      </div>
                    )}
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" onChange={this.onFileChange} />
                        <span className="file">Survey Plan</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button
                        onClick={this.uploadFile}
                        className="upload-btn"
                      >
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Payment Details</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Contract of Sale</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Deed of sublease</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </BodyTemplate>
    );
  }
}

export default AddPlot;
