import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";

class SinglePlot extends Component {
  state = {
    loading: false,
    plot:{},
    plotId:0,
    showErrorBoard:false,
    documents:[],
    payments:[],
    requests:[]
  };

  componentDidMount(){
    const {
      match: { params },
    } = this.props;
    let id = params.id;
    console.log(id);
    this.setState({ plotId: id }, () => this.getPlot());
    
  }    

  getPlot = async () => {
    this.setState({loading:true});
    try{
      let response = await Fetch(`plot/${this.state.plotId}`,'Get');
      let data = await response.json();
      if(data.status){
        console.log(data);
        this.setState({
          loading:false,
          plot:data.data,
          documents:data.data.documents,
          payments:data.data.payments,
          requests:data.data.requests
        });
      }else{
        this.setState({loading:false,showErrorBoard:false});
      }
    }catch(error){
      console.log(error);
      this.setState({loading:false,showErrorBoard:true});
    }
  }

  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div>
          {this.state.plot.appUser ? 
          <div className="row">
          <div className="col-lg-4">
            <div className="joints">
              <h4 className="sub-details">{this.state.plot.appUser.fullName}</h4>
              <h4 className="sub-details">
                User ID: <span> {this.state.plot.appUser.userId}</span>
              </h4>
            </div>
            <h4 className="sub-details">{this.state.plot.appUser.residentialAddress}</h4>
            <h4 className="sub-details">{this.state.plot.appUser.mailingAddress}</h4>
            <div className="joints">
              <h4 className="sub-details">{this.state.plot.appUser.phoneNumber}</h4>
              {/* <h4 className="sub-details">Telephone Number 1</h4> */}
            </div>
          </div>
          <div className="col" />
          <div className="col-lg-2">
            <a href className="res-btn black">
              Residential
            </a>
            <a href className="res-btn orangee">
              Back
            </a>
          </div>
        </div>
        
          : null }
          <div className="row">
            <div className="col-lg-8">
              <div className="row">
                <div className="col-lg-12">
                  <h4 className="plots mb-4 mt-4">Documents</h4>
                </div>
                {this.state.documents.map((document,index) => {
                  return <div className="col-lg-6">
                  <div className="borderonlyfill">
                    <span className="file">{document.documentType}</span>
                    <div className="preview-box">
                      <div className="pay-box"><a target="_blank" href={document.documentName}>Preview</a></div>
                      <i className="fas fa-download" />
                    </div>
                  </div>
                </div>
                
                })

                }
               </div>
            </div>
            <div className="col" />
          </div>
          <div className="dividers" />
          <div className="row">
            <div className="col-lg-6">
            <div className="kin">
            <h4>Payment Timeline</h4>
          </div>
              <div className="pay-title">Payment history</div>
              <div className="surround-table mt-3 mb-4">
                <table className="table table-borderless" width="100%">
                  <thead>
                    <tr className="top">
                      <th className="th-xsm subn">Date </th>
                      <th className="th-lg subn">Details</th>
                      <th className="th-sm subn">Price</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.state.payments ?
          this.state.payments.map((payment,index) => {
            return <tr>
            <td><label htmlFor className="names-group">{new Date(payment.paymentDate).toLocaleDateString()}</label></td>
            <td><label htmlFor className="names-group">{payment.paymentType}</label></td>
            <td><label htmlFor className="names-group">{payment.amount}</label></td>
          </tr>
          })
        : null}</tbody>
                </table>
              </div>
            </div>
            {/* <div className="col-lg-6">
              <div className="pay-title">Payment update</div>
              <div className="surround-table mt-3 mb-4">
                <table className="table table-borderless" width="100%">
                  <thead>
                    <tr className="top">
                      <th className="th-xsm subn">Date </th>
                      <th className="th-lg subn">Details</th>
                      <th className="th-sm subn">Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <label htmlFor className="names-group red-text">
                          12/05/20
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group red-text">
                          First payment
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group red-text">
                          1,000,000.00
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          12/05/20
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          First payment
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          1,000,000.00
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label htmlFor className="names-group red-text">
                          12/05/20
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group red-text">
                          First payment
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group red-text">
                          1,000,000.00
                        </label>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>*/}
            <div className="col-lg-6">
    <div className="kin">
      <h4>Request(s)</h4>
    </div>
    <div className="pay-title">Requests</div>
    <div className="surround-table mt-3 mb-4">
      <table className="table table-borderless" width="100%">
        <thead>
          <tr>
            <th className="th-xsm subn">Date </th>
            <th className="th-lg subn">Request Type</th>
            <th className="th-sm">Plot</th>
            <th className="th-sm subn">Status</th>
          </tr>
        </thead><tbody>
          {this.state.requests ?
          this.state.requests.map((request,index) => {
            return <tr>
            <td><label htmlFor className="names-group">{new Date(request.requestDate).toLocaleDateString()}</label></td>
            <td><label htmlFor className="names-group">{request.requestType}</label></td>
            <td><label htmlFor className="names-info">{request.plotId}</label></td>
            <td><label htmlFor className="names-group">{request.requestStatus}</label></td>
          </tr>
          })
        : 
        <div class="text-center"><span>This user has no requests</span></div>
        }
        </tbody>
      </table>
    </div>
  </div>
  
          </div> 
        </div>
      </BodyTemplate>
    );
  }
}

export default SinglePlot;
