import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import Alert from '../../utilities/Alert';
import Fetch from '../../utilities/Fetch';

class EditPlot extends Component {
  state = {
    plot:{},
    loading:false,
    showErrorBoard:false,
    selectedFile:null
  }
  getPlot = async () => {
    this.setState({loading:true});
    try{
      let response = await Fetch(`plot/${this.state.plotId}`,'Get');
      let data = await response.json();
      if(data.status){
        console.log(data);
        this.setState({
          loading:false,
          plot:data.data,
        });
      }else{
        this.setState({loading:false,showErrorBoard:false});
      }
    }catch(error){
      console.log(error);
      this.setState({loading:false,showErrorBoard:true});
    }
  }

  handleFIleChange = (e) => {
    this.setState({ selectedFile: e.target.files[0] }); 
  }
   render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <Alert/>
        <form onSubmit={this.addPlot}>
          <div>
            <div className="row">
              <div className="col-lg-2">
                <input
                  type="text"
                  id
                  className="signinput-field mb-0"
                  placeholder="Plot Number"
                  name="plotNumber"
                  defaultValue={this.state.plot.plotNumber}
                  onChange={this.handleChanges}
                />
              </div>
              <div className="col-lg-2">
                {/* <input type="text" id="" class="signinput-field mb-0" placeholder="Plot Number"> */}
                <div className="select-plot" onChange={this.handleChanges}>
                  <select id="plot" name="plotTypeId" defaultValue={this.state.plotTypeId}>
                    {this.state.plotTypes.map((type, index) => {
                      return (
                        <option value={type.plotTypeId}>
                          {" "}
                          {type.plotTypeName}
                              </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div className="col-lg-6">
                <input
                  type="text"
                  id
                  className="signinput-field mb-0"
                  placeholder="Plot Address/Location"
                  name="plotAddress"
                  onChange={this.handleChanges}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5">
                <input
                  type="text"
                  id
                  className="signinput-field mb-1"
                  placeholder="Plot Name"
                  name="plotName"
                  onChange={this.handleChanges}
                />
                {/* <div className="tags">
                          <span>Seperate tags with comma</span>
                          <ul className="tags-li">
                          <li>Residential</li>
                          <li>Available</li>
                          </ul>
                        </div> */}
                <div className="row mt-5">
                  <div className="col-lg-6">
                    <h4 className="plots">Square meter</h4>
                    <input
                      type="text"
                      id
                      className="signinput-field mb-0"
                      placeholder="000 sqm"
                      name="size"
                      onChange={this.handleChanges}
                    />
                  </div>
                  <div className="col-lg-6">
                    <h4 className="plots">Price</h4>
                    <input
                      type="text"
                      id
                      className="signinput-field mb-0"
                      placeholder="000,000,000"
                      name="price"
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-lg-12">
                    <h4 className="plots mb-4">Availability</h4>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="pri"
                        defaultChecked
                        name="private"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="pri">Private</label>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="opn"
                        name="open"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="opn">Open for sale</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="ava"
                        name="available"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="ava">Available for bid</label>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="checkk">
                      <input
                        type="checkbox"
                        id="sol"
                        name="sold"
                        onChange={this.handleChanges}
                      />{" "}
                      <label htmlFor="sol">Sold</label>
                    </div>
                  </div>
                </div>
                <div className="row mt-5 mb-5">
                  <div className="col-lg-5">
                    <button className="add-plot-btnn" type="submit">
                      Add
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-lg-5">
                <div className="row mt-3">
                  <div className="col-lg-12">
                    <h4 className="plots mb-4">Upload Documents</h4>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Survey Plan</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Payment Details</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Contract of Sale</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Deed of sublease</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </BodyTemplate>
      );
  }
}

export default EditPlot;
