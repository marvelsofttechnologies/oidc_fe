import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import Spinner from "../../utilities/Spinner";
import { Link } from "react-router-dom";
import Fetch from "../../utilities/Fetch";
import BodyTemplate from "../../utilities/BodyTemplate";

class AllPlots extends Component {
  state = {
    loading: false,
    plots: [],
    error: false,
    errorMessage: "",
    plotSortOption:"",
    allPlots:[]
  };
  componentDidMount() {
    this.listPlots();
  }

  listPlots = () => {
    this.setState({ loading: true });
    Fetch("plots/all", "get")
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          this.setState({ loading: false });
          console.log(data);
        } else {
          this.setState({ loading: false, plots: data.data,allPlots:data.data });
          console.log(data);
        }
      })
      .catch((error) => {
        this.setState({ loading: false });
        console.log(error);
      });
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value }, () => this.sortPlots());
  };

  sortPlots = () =>{
    
    let allPlots = this.state.allPlots;
    let occupiedPlots = allPlots.filter(plot => plot.isOccupied);
    let vacantPlots = allPlots.filter(plot => !plot.isOccupied);
    if(this.state.plotSortOption === 'occupied'){
      this.setState({plots:occupiedPlots});
    }else if(this.state.plotSortOption === 'vacant'){
      this.setState({plots:vacantPlots});
    }else{
      this.setState({plots:allPlots});
    }
  }
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div>
          <div className="row">
            <div className="col-lg-2">
              {" "}
              <Link to="/plots/all" className="vend-btn orange">
                Inventory
              </Link>
            </div>
            <div className="col-lg-2">
              {" "}
              <Link to="/plots" className="vend-btn">
                Statistics
              </Link>
            </div>
            <div className="col-lg-2">
              <div className="customs-select plot-inv">
                <select name="plotSortOption" id onChange={this.handleChanges}>
                  <option value="all">All</option>
                  <option value="occupied">Occupied</option>
                  <option value="vacant">Vacant</option>
                </select>
                <span className="customs-arrow" />
              </div>
            </div>
            <div className="col-lg-2">
              {" "}
              <Link to="/plots/newplot" className="vend-btn green">
                Add Plots
              </Link>
            </div>
          </div>
          <div className="full-width mt-5">
            <table
              id="dtBasicExample"
              className="table subscriber-table table-borderless table-lg "
              cellSpacing={0}
              width="100%"
            >
              <thead>
                <tr className>
                  <th className="th-sm subn">Plots</th>
                  <th className="th-sm subn">Owner</th>
                  <th className="th-sm">Status</th>
                  <th className="th-sm">Price</th>
                  <th className="th-sm">Plot type</th>
                  <th className="th-sm">Sqm</th>
                </tr>
              </thead>
              <tbody>
                {this.state.plots.map((plot, index) => {
                  return (
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          <Link to={`${plot.plotId}`}>{plot.plotName}</Link>
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          {plot.isOccupied ? plot.appUser.fullName : "VACANT"}
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          {plot.status}
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          {plot.price}
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          {plot.plotType}
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          {plot.kilometerSquare}
                        </label>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default AllPlots;
