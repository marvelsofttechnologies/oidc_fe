import React, { Component } from 'react';
import BodyTemplate from '../../utilities/BodyTemplate';

class Communication extends Component {
    state={
        loading:false,
        showErrorBoard:false,
        notice:true,
    }
    render() {
        return (
            <BodyTemplate loading={this.state.loading}showErrorBoard={this.state.showErrorBoard}>
                <div className="row">
  <div className="col-lg-5">
    <div className="request-field">Notice</div>
    <div className="request-field no-borders">Bulk SMS</div>
    <div className="request-field no-borders">Bulk Email</div>
  </div>
  <div className="col" />
  <div className="col-lg-6">
    <form>
      <div className="row">
        <div className="col-lg-12">
          <textarea className="request-field description-field" placeholder="Note" rows={20} defaultValue={""} />
        </div>
        <div className="col-lg-12">
          <div className="row">
            <div className="col" />
            <div className="col-lg-3">
              <a className="vend-btn orange">sEND</a>
            </div>
            <div className="col-lg-3">
              <a className="vend-btn green">SCHEDULE</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>


            </BodyTemplate>
        );
    }
}

export default Communication;