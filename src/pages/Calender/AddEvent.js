import React, { Component } from "react";
import Fetch from "../../utilities/Fetch";
import { MainContext } from "../../contexts/MainContext";
import Alert from "../../utilities/Alert";

class AddEvent extends Component {
  static contextType = MainContext;
  state = {
    title: "",
    date: "",
    time: "",
    location: "",
    description: "",
    fullName: "",
    adding: false,
    eventTypeId:0,
    events:[]
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  componentDidMount= () =>{
    this.loadEvents()
  }

  addEvent = (e) => {
    let newEvent = {
      title: this.state.title,
      date: this.state.date,
      startDate: this.state.startTime,
      endDate: this.state.endTime,
      location: this.state.location,
      description: this.state.description,
      name: this.state.fullName,
      eventTypeId: this.state.eventTypeId,
      note: this.state.time,
    };

    this.submitEvent(newEvent);
    console.log(this.state);
    e.preventDefault();
  };

  submitEvent = async (event) => {
    
    try{const context = this.context;
    let res = await Fetch("calendar", "post", event);
    let data = await res.json();
    console.log(data);
    if (data.status) {
      context.showAlert("success", "Event Added Successfully", "");
      // window.reload();
    }else{
      context.showAlert(
        "danger",
        data.message.toString(),
        data.message.toString()
      );
    }
  }catch(error){
      console.log(error);
    }
  };

  loadEvents = async () => {
   try{
    let response = await Fetch("calendar/events", "GET");
    let data = await response.json();
    console.log(data)
    this.setState({events:data.data});
   }catch(error){
     console.log(error);
   }
          
        
  };

  render() {
    return (
      <>
        <Alert />
        <form onSubmit={this.addEvent}>
          <input
            type="text"
            id
            className="signinput-field mb-1"
            placeholder="Add title"
            naem="title"
            onChange={this.handleChanges}
          />
          <select style={{borderTop: 'none',borderLeft: "none",borderRight: "none"}} name="eventTypeId" onChange={this.handleChanges} className=" custom-select mt-5">
            {this.state.events.map((event,index) =>{
              return <option value={event.eventId}>{event.name}</option>
            })}
          </select>
          <div className="tags" style={{width:'700px',height:'5px'}}>
            {/* <span>Seperate tags with comma</span>
            <ul className="tags-li">
              <li className="greeen">Meeting</li>
              <li className="redd">Payment</li>
              <li className="orangee">Payment</li>
              <li className="ashh">Payment</li>
            </ul> */}
          </div>
          <div className="fieldwithicon">
            <i className="far fa-calendar-check" />
            <input
              type="date"
              id
              className="signinput-field mb-1"
              placeholder="22, November, 2020"
              name="date"
              onChange={this.handleChanges}
            />
          </div>
          <div className="fieldwithicon">
            <i className="far fa-clock" />
            <input
              type="time"
              id
              className="signinput-field mb-1"
              placeholder="00.00am (start time)"
              name="startTime"
              onChange={this.handleChanges}
            />
          </div>
      
          <div className="fieldwithicon">
            <i className="fas fa-map-marker-alt" />
            <input
              type="text"
              id
              className="signinput-field mb-1"
              placeholder="Add location"
              name="location"
              onChange={this.handleChanges}
            />
          </div>
          <div className="fieldwithicon">
            <i className="fas fa-sort-amount-down-alt" />
            <input
              type="text"
              id
              className="signinput-field mb-1"
              placeholder="Add description"
              name="description"
              onChange={this.handleChanges}
            />
          </div>
          <div className="fieldwithicon">
            <i className="fas fa-users" />
            <input
              type="text"
              id
              className="signinput-field mb-1"
              placeholder="With who?"
              name="fullName"
              onChange={this.handleChanges}
            />
          </div>
          <button type="submit" className="vend-btn green mt-3">
            Add
          </button>
        </form>
      </>
    );
  }
}

export default AddEvent;
