import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import Fetch from "../../utilities/Fetch";
import Modal from "../../utilities/Modal";
import AddEvent from "./AddEvent";
import BodyTemplate from '../../utilities/BodyTemplate';

class Calender extends Component {
  state ={
    loading:false,
    events:[],
    defaultEvents:[],
    months :["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  }
  componentDidMount() {
    this.loadEvents();
  }
  handleEventClick = ({ event }) => {
    // openAppointment is a function I wrote to open a form to edit that appointment
    this.props.openAppointment(event.extendedProps);
  };
  handleDateClick = (arg) => {
    // bind with an arrow function
    alert(arg.dateStr);
    console.log(arg);
  };

  loadEvents = () => {
    Fetch("calendar/events", "GET")
      .then((res) => res.json())
      .then((data) => {
        let events = []
        data.data.forEach(element => {
          let singleEvent = {
            title:element.name,
            start:element.dateCreated,
            end:element.dateModified
          };
          events.push(singleEvent);
        });
        this.setState({events:events,defaultEvents:data.data});
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleEventDrop = (info) => {
    if (window.confirm("Are you sure you want to change the event date?")) {
      console.log("change confirmed");

      // updateAppointment is another custom method
      this.props.updateAppointment({
        ...info.event.extendedProps,
        start: info.event.start,
        end: info.event.end,
      });
    } else {
      console.log("change aborted");
    }
  };
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <Modal id={"AddEventModal"} title={"Add Event"}>
          <AddEvent />
        </Modal>
        <div className="row mb-4">
          <div className="col-lg-2">
            <div className="month-title">{this.state.months[new Date().getMonth()]}</div>
            <div className="calendar-title black-color">
              2020 <span> </span>
            </div>
          </div>
          <div className="offset-8 col-lg-2">
            <div className="activy">
              <div className="calendar-box blackk px-5 mb-0">
                <button
                  type="button"
                  className="no-bg"
                  data-toggle="modal"
                  data-target="#AddEventModal"
                >
                  <div className="activity">Add activity</div>
                </button>
              </div>
            </div>
          </div>
        </div>

        <FullCalendar
          headerToolbar={{
            start: "", // will normally be on the left. if RTL, will be on the right
            center: "",
            end: "", // will normally be on the right. if RTL, will be on the left
          }}
          plugins={[dayGridPlugin, interactionPlugin]}
          editable={true}
          eventDrop={this.handleEventDrop}
          eventClick={this.handleEventClick}
          initialView="dayGridMonth"
          h
          events={this.state.events}
          eventColor="#fff"
          eventBackgroundColor=""
          eventBorderColor="#D86018"
          eventTextColor="black"
          dateClick={this.handleDateClick}
          viewClassNames={{}}
        />

        <div className="row mt-5">
          <div className="col-lg-7">
            <div className="row">
              <div className="col-lg-4">
                <div className="calendar-box greeen px-5">
                  <div className="activity">Today activity</div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <div className="calendar-box ashh px-4">
                  <div className="description">Time</div>
                </div>
              </div>
              <div className="col-lg-9">
                <div className="calendar-box ashh">
                  <div className="description px-4">description</div>
                </div>
              </div>
            </div>
            {this.state.defaultEvents.map((event,index)=> {
                return <div className="row">
                <div className="col-lg-3">
                  <div className="calendar-box single-calendar box-shadow d-flex-column align-items-center">
                    <div className="calendar-info currently mb-2">{new Date(event.dateCreated).toLocaleTimeString()}</div>
                    {/* <div className="calendar-info incoming">09:00am</div> */}
                  </div>
                </div>
                <div className="col-lg-9">
                  <div className="calendar-box greeen single-calendar d-flex-column justify-content-between">
                    <div className="title-box">
                      <div className="calendar-title px-3">
                        {event.name}
                      </div>
                      <div className="calendar-info px-3">{event.description}</div>
                    </div>
                    <div className="other-info px-3">
                      <div className="single-item">
                        <div className="contact-icon">
                          <i className="fas fa-map-marker-alt" />
                        </div>
                        <div className="calendar-info">Zoom meeting</div>
                      </div>
                      <div className="single-item">
                        <div className="contact-icon">
                          <img src="/Assets/Avatar.png" alt />
                        </div>
                        <div className="calendar-info">Contact Person</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            })}
            {/* <div className="row">
              <div className="col-lg-2">
                <div className="calendar-box single-calendar box-shadow d-flex-column align-items-center">
                  <div className="calendar-info currently mb-2">08:00am</div>
                  <div className="calendar-info incoming">09:00am</div>
                </div>
              </div>
              <div className="col-lg-10">
                <div className="calendar-box redd single-calendar d-flex-column justify-content-between">
                  <div className="title-box">
                    <div className="calendar-title px-3">
                      PAYMENT DEADLINE (Send message)
                    </div>
                    <div className="calendar-info px-3">Payment review</div>
                  </div>
                  <div className="other-info px-3">
                    <div className="single-item">
                      <div className="contact-icon">
                        <img src="/Assets/Avatar.png" alt />
                      </div>
                      <div className="calendar-info">Contact Person</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-2">
                <div className="calendar-box single-calendar box-shadow d-flex-column align-items-center">
                  <div className="calendar-info currently mb-2">08:00am</div>
                  <div className="calendar-info incoming">09:00am</div>
                </div>
              </div>
              <div className="col-lg-10">
                <div className="calendar-box ashh single-calendar d-flex-column justify-content-between">
                  <div className="title-box">
                    <div className="calendar-title px-3">
                      MOBILIZATION CHECK (Plot 25)
                    </div>
                    <div className="calendar-info px-3">
                      A &amp;A contractors coming on site
                    </div>
                  </div>
                  <div className="other-info px-3"></div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-2">
                <div className="calendar-box single-calendar box-shadow d-flex-column align-items-center">
                  <div className="calendar-info currently mb-2">08:00am</div>
                  <div className="calendar-info incoming">09:00am</div>
                </div>
              </div>
              <div className="col-lg-10">
                <div className="calendar-box orangee single-calendar d-flex-column justify-content-between">
                  <div className="title-box">
                    <div className="calendar-title px-3">SITE VISIT</div>
                    <div className="calendar-info px-3">With new client</div>
                  </div>
                  <div className="other-info px-3">
                    <div className="single-item">
                      <div className="contact-icon">
                        <img src="/Assets/Avatar.png" alt />
                      </div>
                      <div className="calendar-info">Contact Person</div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default Calender;
