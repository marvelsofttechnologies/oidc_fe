import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";
import {Link} from 'react-router-dom';

class Requests extends Component {
  state = {
    loading: false,
    requests: [],
    requestSortOption:'string',
    allRequests:[]
  };

  componentDidMount() {
    this.listRequests();
  }
  listRequests = () => {
    this.setState({ loading: true });
    Fetch("request", "Get")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          this.setState({ requests: data.data,allRequests:data.data,loading:false });
        } else {
          this.setState({ loading: false });
        }
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
      });
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value },() => this.sortRequests());
  };

  sortRequests = () => {
    let allRequests = this.state.allRequests;
    let approvedRequests = allRequests.filter(request => request.requestStatus == "APPROVED");
    let pendingRequests = allRequests.filter(request => request.requestStatus == "PENDING");
    let rejectedRequests = allRequests.filter(request => request.requestStatus == "REJECTED");
    if(this.state.requestSortOption === "all"){
      this.setState({requests:allRequests});
    }else if(this.state.requestSortOption === "rejected"){
      this.setState({requests:rejectedRequests});
    }else if(this.state.requestSortOption === "pending"){
      this.setState({requests:pendingRequests});
    }else if(this.state.requestSortOption === "pending"){
      this.setState({requests:approvedRequests});
    }
  }
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div className="row">
          {/* <div class="col-lg-2"> <a class="vend-btn orange">Incoming Requests</a></div> */}
          {/* <div class="col-lg-2"> <a class="vend-btn">Proposals</a></div> */}
          <div className="col-lg-2">
            <div className="customs-select plot-inv">
              <select name='paymentSortOption' onChange={this.handleChanges}>
                <option value="all">All</option>
                <option value="approved">Approved</option>
                <option value="pending">Pending</option>
                <option value="rejected">Rejected</option>
              </select>
              <span className="customs-arrow" />
            </div>
          </div>
          {/* <div class="col-lg-2"> <a class="vend-btn green">Add Vendor</a></div> */}
        </div>
        <div className="full-width ">
          <div className="pagenation">
            <div className="left-arrow">
              <i className="fas fa-angle-left" />
              <i className="fas fa-angle-left" />
            </div>
            <div className="page-number">1-50</div>
            <div className="right-arrow">
              <i className="fas fa-angle-right" />
              <i className="fas fa-angle-right" />
            </div>
          </div>
          <table
            id="dtBasicExample"
            className="table subscriber-table table-borderless table-lg "
            cellSpacing={0}
            width="100%"
          >
            <thead>
              <tr className>
                <th className="th-sm subn">Date</th>
                <th className="th-sm subn">Name</th>
                <th className="th-sm subn">Nature</th>
                <th className="th-sm subn">Plot No</th>
                <th className="th-sm subn">Email</th>
                <th className="th-sm subn">Phone Number</th>
                <th className="th-sm subn">Status</th>
              </tr>
            </thead>
            {this.state.requests.map((request, index) => {
              return (
                <tbody>
                  <tr>
                    <td>
                      <label htmlFor className="names-group">
                        <Link to={`requests/${request.requestId}`}>
                        {new Date(request.dateCreated).toLocaleDateString()}
                        </Link>
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        George Fields
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        {request.requestName}
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        12
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        georgiafields@gmail.com
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        090123456789
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-group">
                        {request.requestStatus}
                      </label>
                    </td>
                  </tr>
                </tbody>
              );
            })}
          </table>
        </div>
      </BodyTemplate>
    );
  }
}

export default Requests;
