import React, { Component } from "react";
import Spinner from "../../utilities/Spinner";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";
import Alert from "../../utilities/Alert";

class SingleRequest extends Component {
  static contextType = MainContext;
  state = {
    loading: false,
    requestId: 0,
    request: {},
    processing: false,
    user:{},
    plot:{}
  };

  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    let id = params.id;
    console.log(id);
    this.setState({ requestId: id }, () => this.getDetails());
  }

  getDetails = async () => {
    this.setState({ loading: true });
    try {
      let response = await Fetch(`request/${this.state.requestId}`, "Get");
      let data = await response.json();
      console.log(data);
      this.setState({ request: data.data, loading: false,user:data.data.appUser, plot:data.data.plot });
    } catch (error) {
      console.log(error);
    }
  };

  decline = async () => {
    const context = this.context;
    this.setState({ processing: true });
    try {
      let response = await Fetch(
        `request/decline/${this.state.requestId}`,
        "Get"
      );
      let data = await response.json();
      if (data.status) {
        context.showAlert("success", "Request Declined", "Success");
      } else {
        context.showAlert("danger", "An Error Occured", "Success");
      }
    } catch (error) {
      console.log(error);
    }
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  approve = async () => {
    const context = this.context;
    this.setState({ processing: true });
    try {
      let response = await Fetch(
        `request/approve/${this.state.requestId}`,
        "Get"
      );
      let data = await response.json();
      if (data.status) {
        context.showAlert("success", "Request Approved", "Success");
      } else {
        context.showAlert("danger", "An Error Occured", "Success");
      }
    } catch (error) {
      console.log(error);
    }
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  reschedule = async () => {};
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <Alert />
        <div className="row">
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-10">
                    <div className="request-field">
                      {this.state.request.requestType}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-5">
                    <div className="request-field">{this.state.user.fullName}</div>
                  </div>
                  <div className="col-lg-3">
                    <div className="request-field">{this.state.plot.plotId}</div>
                  </div>
                </div>
              </div>
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-5">
                    <div className="request-field">
                      {this.state.request.requestName}
                    </div>
                  </div>
                  <div className="col-lg-3">
                    <div className="request-field">
                      {new Date(this.state.request.dateCreated)
                        .toLocaleDateString()
                        .toString()}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-12">
                <div className="request-field description-field">
                  {this.state.request.description}
                </div>
              </div>
              <div className="col-lg-12">
                <div className="row mb-4">
                  <div className="col-lg-3">
                    <a className="vend-btn black" onClick={this.decline}>
                      Decline
                    </a>
                  </div>
                  <div className="col-lg-3">
                    <a className="vend-btn orange" onClick={this.approve}>
                      Accept
                    </a>
                  </div>
                  <div className="col-lg-3">
                    <a className="vend-btn green" onClick={this.reschedule}>
                      Reschedule
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-12">
                <form>
                  <div className="row">
                    <div className="col-lg-12">
                      <textarea
                        className="request-field description-field"
                        placeholder="Note"
                        defaultValue={""}
                      />
                    </div>
                    <div className="col-lg-3 offset-9 mb-5">
                      <a className="vend-btn orange">Respond</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default SingleRequest;
