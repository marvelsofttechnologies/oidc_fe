import React, { Component } from "react";
import {Link} from 'react-router-dom'
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";

class Offers extends Component {
  state = {
    loading: false,
    showErrorBoard: false,
    offers: [],
  };

  componentDidMount() {
    this.listOffers();
  }

  listOffers = async () => {
    this.setState({ loading: true });
    try {
      var response = await Fetch("offers", "GET");
      response = await response.json();
      if (response.status) {
        console.log(response);
        this.setState({ offers: response.data, loading: false });
      }
    } catch (error) {
      console.log(error);
      this.setState({ loading: false });
    }
  };

  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <div>
            <div className="kins">
              <h4>Available</h4>
            </div>
            <div className="full-width mt-5">
              <table
                id="dtBasicExample"
                className="table subscriber-table table-borderless table-lg "
                cellSpacing={0}
                width="100%"
              >
                <thead>
                  <tr className>
                    <th className="th-sm subn">Date </th>
                    <th className="th-sm">Plot ID</th>
                    <th className="th-sm">Plot Size</th>
                    <th className="th-sm">Plot Type</th>
                    <th className="th-sm">Status</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.offers.map((offer,index) => {
                    return                   <tr>
                    <td>
                      <label className="names-group">
                        {offer.plotId}
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-info">
                      {offer.plotId}
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-info">
                        {offer.plot.kilometerSquare}
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-info">
                        {offer.plot.plotType}
                      </label>
                    </td>
                    <td>
                      <label htmlFor className="names-info">
                        {offer.offerStatus}  
                      </label>
                    </td>
                  </tr>
                  })}
                </tbody>
              </table>
            </div>

            <div className="d-flex mt-3">
              <div className="create-btn">
                <Link to="/offers/add">Create offer</Link>
              </div>
            </div>
          </div>
        </BodyTemplate>
      </>
    );
  }
}

export default Offers;
