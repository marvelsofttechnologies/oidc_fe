import React, { Component } from "react";
import PlotAllocation from "../../components/Charts/PlotAllocation";
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";
import { MainContext } from "../../contexts/MainContext";
import Alert from "../../utilities/Alert";

class CreateOffer extends Component {
  static contextType = MainContext;
  state = {
    loading: false,
    showErrorBoard: false,
    availablePlots: [],
    subscribers: [],
    plotId: 0,
    documentPath: "",
    userId: 0,
    initialAmount: 0,
    sellingPrice: 0,
    durationInMonths: 0,
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  componentDidMount() {
    this.listAvailabePlots();
    this.listSubscribers();
  }

  listAvailabePlots = () => {
    this.setState({ loading: true });
    Fetch("plots/available", "GET")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.status) {
          this.setState({ availablePlots: data.data, loading: false });
        } else {
          console.log(data.error);
          this.setState({ loading: false });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  listSubscribers = () => {
    this.setState({ loading: true });
    Fetch("metrics/subscribers/existing", "GET")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          this.setState({ subscribers: data.data });
          this.setState({ loading: false });
        } else {
          this.setState({ errorMessage: data.errors[0] });
          this.setState({ loading: false });
        }
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
      });
  };

  handleSubmit = (e) =>{
    const context = this.context;
    let newOffer = {
      plotId: this.state.plotId,
      "documentPath": "",
      "userId": this.state.userId,
      "initialAmount": this.state.initialAmount,
      "sellingPrice": this.state.sellingPrice,
      "durationInMonths": this.state.durationInMonths
    };
    console.log(newOffer);
    this.setState({creating:true});
    Fetch("offer","Post",newOffer)
    .then(response => response.json())
    .then(data => {
      console.log(data);
      if(data.status){
        context.showAlert("success", "Offer created Successfully", "Success");
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
          window.location.href = "/offers"
      }else{
        context.showAlert("danger", "An error occured while creating this offer", "Error");
        window.scroll({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
      }
    })
    .catch(error => {
      context.showAlert("danger", "An error occured while creating this offer", "Error");
      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
      console.log(error);
    })

    e.preventDefault();
  }
  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <Alert/>
          <form onSubmit={this.handleSubmit}>
          <div>
            <div className="row">
              <div className="col-lg-4">
                <div className="each-sec">
                  <div className="kins d-flex">
                    <h4>Plot name</h4>{" "}
                    <span className="sqm ml-3">(XYZ sqm)</span>
                  </div>
                  <div className="d-flex align-items-center">
                    <div className="row">
                      <div className="col">
                        <select
                          id="plot"
                          name="plotId"
                          onChange={this.handleChanges}
                        >
                          <option>Select Plot</option>
                          {this.state.availablePlots.map((plot, index) => {
                            return (
                              <option value={plot.plotId}>
                                {plot.plotName}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                      <div className="col">
                        <select
                          id="plot"
                          name="userId"
                          onChange={this.handleChanges}
                        >
                          <option>Select Subscriber</option>
                          {this.state.subscribers.map((subscriber, index) => {
                            return (
                              <option value={subscriber.userId}>
                                {subscriber.fullName}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                    {/* <div class="ads-btn"><a href="#">Add</a></div> */}
                  </div>
                </div>
                <div className="each-sec">
                  <div className="kins d-flex">
                    <h4>Selling price</h4>
                  </div>
                  <div className="d-flex align-items-center">
                    <input
                      type="text"
                      className="input-border"
                      placeholder="1,000,000,000.00"
                      name="initialAmount"
                      onChange={this.handleChanges}
                    />
                    {/* <div class="ads-btn"><a href="#">Add</a></div> */}
                  </div>
                </div>
              </div>
              <div className="col-lg-1" />
              <div className="col-lg-6 mt-4">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Survey Plan</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="upload-section mb-4">
                      <label className="borderonly">
                        <input type="file" />
                        <span className="file">Survey Plan</span>
                        <i className="fas fa-upload" />
                      </label>
                      <button type="submit" className="upload-btn">
                        Upload
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6">
                <div className="each-sec">
                  <div className="kins d-flex">
                    <h4>Price with instalment payment</h4>
                  </div>
                  <div className="d-flex align-items-center">
                    <input
                      type="text"
                      className="input-border"
                      placeholder="5,000,000,000.00"
                      name="sellingPrice"
                      onChange={this.handleChanges}
                    />
                    <p className="side-text">Naira</p>
                    <input
                      type="text"
                      className="input-border w-10"
                      placeholder={12}
                      name="durationInMonths"
                      onChange={this.handleChanges}
                    />
                    <p className="side-text">Months</p>
                    {/* <div class="ads-btn"><a href="#">Add</a></div> */}
                  </div>
                </div>
                <div className="each-sec">
                  <div className="kins d-flex">
                    <h4>First payment</h4>
                  </div>
                  <div className="d-flex align-items-center">
                    <input
                      type="text"
                      className="input-border"
                      placeholder="1,000,000,000.00"
                      name="initialPrice"
                      onChange={this.handleChanges}
                    />
                    {/* <div class="ads-btn"><a href="#">Add</a></div> */}
                  </div>
                </div>

                <button type="submit" className="upload-btn">
                  Create offer
                </button>
              </div>
            </div>
          </div>
          </form>
        </BodyTemplate>
      </>
    );
  }
}

export default CreateOffer;
