import React, { Component } from "react";
import Fetch from '../utilities/Fetch'
import Spinner from '../utilities/Spinner'
import {MainContext} from '../contexts/MainContext'
import {Redirect} from 'react-router-dom';
import { Link } from "react-router-dom/cjs/react-router-dom.min";

class SIgnIn extends Component {
    static contextType = MainContext;
    state={
        email:"",
        password:"",
        loading:false,
        user: {},
        succeeded:false
    }

    UpdateUser = (user) => {
      let context = this.context;
      context.updateUser(user);
      }

      componentDidMount(){
        let context = this.context;
       this.setState({user:context.user,setUser:context.setUser});
    }
    handleChanges = (e) => {
        const input = e.target;
        const name = input.name;
        const value = input.type === "checkbox" ? input.checked : input.value;
        this.setState({ [name]: value });
      };
    
      handleSubmit = (e) => {
        this.setState({ loading: true });
        var data = {
          email: this.state.email,
          password: this.state.password,
          rememberMe: false,
        };

        Fetch("admin/token", "post", data)
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data.status) {
              localStorage.setItem("user", JSON.stringify(data.data));
              localStorage.setItem("token", JSON.stringify(data.data.token));
              localStorage["token"] = data.data.token;
              this.UpdateUser(data.data);
              this.setState({succeeded:true})
            } else {
              this.setState({ errorMessage: data.message });
            }
            this.setState({ loading: false });
          })
          .catch((error) => {
            console.error(error);
          });
        e.preventDefault();
      };

  render() {
    return this.state.succeeded ? 
    <Redirect to="/app" />
    :(
      <div className="wrapper">
        <div className="inner-content">
          <div className="logop">
            <img src="./Assets/orangelogo.png" alt />
          </div>
          <div id="sign-in-form">
            <form className="log-form" onSubmit={this.handleSubmit}>
              <h3 className="login">Log in</h3>
              {this.state.errorMessage ? 
              <div className="text-center">
              <p className="text-danger">{this.state.errorMessage}</p>
            </div>
            : null}
              <input
                type="email"
                placeholder="E-mail"
                className="input-field"
                name="email"
                onChange={this.handleChanges}
                required
              />
              <input
                type="password"
                placeholder="Password"
                className="input-field"
                name="password"
                onChange={this.handleChanges}
                required
              />
              <button className="sign-btns">{this.state.loading ?<Spinner size={'small'} loading={this.state.loading} /> : "Log In"}</button>
              <span>
                <Link to="/login/reset"> Forgotten Password?</Link>
              </span>
            </form>
          </div>
          <div className="footer">
            <ul>
              <li>
                <a href="#">Privacy policy</a>
              </li>
              <li>
                <a href="#">Documentation</a>
              </li>
              <li>
                <a href="#">Our website</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default SIgnIn;
