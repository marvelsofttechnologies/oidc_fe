import React, { Component } from "react";
import BodyTemplate from "../../utilities/BodyTemplate";

class Admis extends Component {
  state = {
    loading: false,
    showErrorBoard: false,
  };
  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        ></BodyTemplate>
      </>
    );
  }
}

export default Admis;
