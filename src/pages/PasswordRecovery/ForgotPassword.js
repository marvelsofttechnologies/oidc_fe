import React, { Component } from "react";
import Footer from "../../components/Navigations/Footer";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";
import {Link} from 'react-router-dom';

class ForgotPassword extends Component {
  state = {
    emailSent: false,
    email:"",
    loading:false,
    platform:"web",
    errorMessage:"",
    notVerified:false
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) =>{
      this.setState({loading:true})
      console.log(this.state.email);
      Fetch(`admin/reset?email=${this.state.email}&platform=${this.state.platform}`,"GET")
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if(data.status){
          this.setState({emailSent:true});
        }else{
          this.setState({loading:false})
          this.setState({errorMessage:data.message});
          if(data.message === "Email has not been confirmed, kindly confirm."){
            this.setState({notVerified:true});
          }
        }
      })
      .catch(error => {
        this.setState({loading:false})
        console.log(error);
      })
    e.preventDefault();
  }

  requestLink = async () => {
    this.setState({loading:true})
    try{
      let response = await Fetch(`admin/request-otp?email=${this.state.email}&platform=web`,'get');
      let data =await response.json();
      console.log(data);
      if(data.status){
        console.log(data)
        this.setState({loading:false,emailSent:true})
      }else{
        this.setState({loading: false,errorMessage:"An error occured please contact admin"})
      }
    }catch(error){
      console.error(error);
    }
  }

  setAction = async (step) => {
    if(step = 0){
      this.handleSubmit();
    }else{
     await  this.requestLink()
    }
  }

  render() {
    return (
      <>
        <div className="wrapp">
          <div className="inner-content">
            <div className="logop d-flex justify-content-start ml-5">
              <img src="/Assets/orangelogo.png" alt />
            </div>
          </div>
          <div className="box d-flex justify-content-center">
            <div className="boxes">
              {!this.state.emailSent ? (
                <div className="box-inner">
                  <div className="icons">
                    <img src="/Assets/12135-email-send 1.png" alt />
                  </div>
                  <div className="title">
                    <h3 className="m-0">Forgot Password</h3>
                  </div>
                  <div className="info">
                    {this.state.errorMessage ? 
                   <>
                    <p className="text-danger">
                    {this.state.errorMessage}
                   </p>
                  </>
                  :
                  <p>
                      Please enter the e-mail address associated with your
                      account
                    </p>
                  }
                    
                  </div>
                  <form onSubmit={this.handleSubmit}>
                    <input
                      type="email"
                      name="email"
                      placeholder="E-mail"
                      className="input-field forgot"
                      onChange={this.handleChanges}
                      required
                    />
                    {this.state.loading ?
                <button className="sign-btns forget disabled" type="submit"><Spinner size={"small"}/></button>
                : this.state.notVerified ? <div className="sign-btns forget" onClick={async () => {await this.requestLink()}}>Request new Link</div>   :
                <button className="sign-btns forget" type="submit">Confirm</button>    
                }
                  </form>
                </div>
              ) : (
                <div className="box-inner py-4">
                  <div className="icons">
                    <img src="/Assets/12135-email-send 1.png" alt />
                  </div>
                  <div className="title">
                    <h3 className="m-0">Check Your email</h3>
                  </div>
                  <div className="info">
                    <p>
                      Please check your email <span>janedoe@gmail.com </span>
                      You will recieve a verification link link.
                    </p>
                  </div>
                  <form>
                    <button className="sign-btns forget">
                      RETURN TO LOGIN
                    </button>
                  </form>
                </div>
              )}
            </div>
          </div>
          <Footer />
        </div>
      </>
    );
  }
}

export default ForgotPassword;
