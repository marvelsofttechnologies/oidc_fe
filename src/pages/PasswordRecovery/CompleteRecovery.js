import { logDOM } from "@testing-library/react";
import React, { Component } from "react";
import { Link,Redirect } from "react-router-dom";
import Footer from "../../components/Navigations/Footer";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";

class CompleteRecovery extends Component {
  state = {
    resetDone: false,
    password: "",
    confirmPassword: "",
    code: "",
    loading: false,
    platform: "web",
  };

  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    var OTP = params.code;
    this.setState({
      code: OTP,
    });
  }
  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    this.setState({ loading: true });
    console.log(
      `${this.state.code},${this.state.password},${this.state.confirmPassword}`
    );
    var data = {
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      otpCode: this.state.code,
      platform: this.state.platform,
    };
    Fetch("admin/complete-reset", "PUT", data)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
      });
    e.preventDefault();
  };
  render() {
    
    return (
      <>
        <div className="wrapp">
          <div className="inner-content">
            <div className="logop d-flex justify-content-start ml-5">
              <img src="/Assets/orangelogo.png" alt />
            </div>
          </div>
          <div className="box d-flex justify-content-center">
            <div className="boxes">
              {!this.state.resetDone ? (
                <div className="box-inner py-3">
                  <div className="icons">
                    <img
                      src="/Assets/23668-key-locker-privacy-password-icon 1.png"
                      alt
                    />
                  </div>
                  <div className="title">
                    <h3 className="m-0">Set New Password</h3>
                  </div>
                  <form onSubmit={this.handleSubmit}>
                    <input
                      name="password"
                      type="password"
                      placeholder="New Password"
                      className="input-field forgot"
                      onChange={this.handleChanges}
                      required
                    />
                    <input
                      name="confirmPassword"
                      type="password"
                      placeholder="Confirm New Password"
                      className="input-field forgot"
                      onChange={this.handleChanges}
                      required
                    />
                    <div className="form-btn d-flex justify-content-between mt-2">
                      {this.state.loading ? (
                        <button className="sign-btns fit disabled">
                          <Spinner size={"small"} />
                        </button>
                      ) : (
                        <button className="sign-btns fit" type="submit">
                          Confirm
                        </button>
                      )}
                      <button className="sign-btns fit">
                        <Link to="/login" className="sign-btns fit">
                          Login
                        </Link>
                      </button>
                    </div>
                  </form>
                </div>
              ) : (
                <div className="box-inner py-4">
                  <div className="icons">
                    <img src="/Assets/12135-email-send 1.png" alt />
                  </div>
                  <div className="title">
                    <h3 className="m-0">Account Recovery Complete</h3>
                  </div>
                  <div className="info">
                    <p>
                      Your password has been successfully reset. Please proceed
                      to login thank you.
                    </p>
                  </div>
                  <form>
                    <button className="sign-btns forget">
                      <Link to="/login" className="sign-btns forget">
                        RETURN TO LOGIN
                      </Link>
                    </button>
                  </form>
                </div>
              )}
            </div>
          </div>
          <Footer />
        </div>
      </>
    );
  }
}

export default CompleteRecovery;
