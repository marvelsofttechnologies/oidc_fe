import React, { Component } from "react";
import IncomeFlow from "../../components/Charts/IncomeFlow";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import Spinner from "../../utilities/Spinner";

class Payments extends Component {
  state = {
    loading: false,
  };
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        <div className="row">
          <div className="col-lg-8">
            <div className="row">
              <div className="col-lg-6">
                <div className="selecting d-flex">
                  <div className="customs-select">
                    <select id>
                      <option value>Total Payment</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                    </select>
                    <span className="customs-arrow" />
                  </div>
                  {/* <p>Payment Status</p> */}
                  <div className="customs-select">
                    <select id>
                      <option value>This Month</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                      <option value>Default Payment</option>
                    </select>
                    <span className="customs-arrow" />
                  </div>
                  {/* <p>Development Level</p> */}
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="payment-box">
                  <h5 className="payment">Payment History</h5>
                  <div className="payment-box-inner">
                    <div className="box-top d-flex justify-content-between w-100">
                      <h3 className="payment">Income flow</h3>
                      <p className="bl">
                        {" "}
                        <label className="circle" />
                        2020
                      </p>
                    </div>
                    <div className="payment-big-chart">
                      <IncomeFlow />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="row">
              <div className="col-lg-12">
                <div className="plot-box">
                  <div className="box-title d-flex w-100 justify-content-between">
                    <h5 className="plot-title">Plot Allocation</h5>
                  </div>
                  <div className="chart-container-2"></div>
                  <h5 className="plot-bottom-text">
                    Plot Allocation already in use
                  </h5>
                </div>
              </div>
              <div className="col-lg-12">
                <div className="overdue-box">Overdue payments</div>
                <div className="overdues">
                  <div className="table-head-div mt-3">
                    <table className="subscriber-table">
                      <thead>
                        <tr className>
                          <th>
                            {" "}
                            <label htmlFor className="top-labels mr4 bold-text">
                              Subscriber
                            </label>
                          </th>
                          <th>
                            {" "}
                            <label
                              htmlFor
                              className="top-labels mr-0 bold-text"
                            >
                              Contact
                            </label>
                          </th>
                        </tr>
                      </thead>
                    </table>
                    <div className="full-width overdue-height">
                      <table className="sub-table ">
                        <tbody>
                          <tr>
                            <td>
                              <div className="names-group mt-2">
                                <img
                                  src="/Assets/Avatar.png"
                                  alt
                                  className="img-circle lbl"
                                />
                                <label htmlFor="t1" className="names mr3">
                                  Lindsey stroud
                                </label>
                              </div>
                            </td>
                            <td>
                              <label htmlFor className="names-info mr-0 mt-2">
                                08088749339
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div className="names-group mt-2">
                                <img
                                  src="/Assets/Avatar.png"
                                  alt
                                  className="img-circle lbl"
                                />
                                <label htmlFor="t1" className="names mr3">
                                  Lindsey stroud
                                </label>
                              </div>
                            </td>
                            <td>
                              <label htmlFor className="names-info mr-0 mt-2">
                                08088749339
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div className="names-group mt-2">
                                <img
                                  src="/Assets/Avatar.png"
                                  alt
                                  className="img-circle lbl"
                                />
                                <label htmlFor="t1" className="names mr3">
                                  Lindsey stroud
                                </label>
                              </div>
                            </td>
                            <td>
                              <label htmlFor className="names-info mr-0 mt-2">
                                08088749339
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div className="names-group mt-2">
                                <img
                                  src="/Assets/Avatar.png"
                                  alt
                                  className="img-circle lbl"
                                />
                                <label htmlFor="t1" className="names mr3">
                                  Lindsey stroud
                                </label>
                              </div>
                            </td>
                            <td>
                              <label htmlFor className="names-info mr-0 mt-2">
                                08088749339
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div className="names-group mt-2">
                                <img
                                  src="/Assets/Avatar.png"
                                  alt
                                  className="img-circle lbl"
                                />
                                <label htmlFor="t1" className="names mr3">
                                  Lindsey stroud
                                </label>
                              </div>
                            </td>
                            <td>
                              <label htmlFor className="names-info mr-0 mt-2">
                                08088749339
                              </label>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </BodyTemplate>
    );
  }
}

export default Payments;
