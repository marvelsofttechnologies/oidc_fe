import React, { Component } from "react";
import { Link } from "react-router-dom";
import BodyTemplate from "../../utilities/BodyTemplate";
import Fetch from "../../utilities/Fetch";

class Proposals extends Component {
  state = {
    loading: false,
    showErrorBoard: false,
    proposals:[]
  };

componentDidMount(){
  this.listProposals();
}

  listProposals = () =>{
    this.setState({loading:true})
    Fetch("proposals","get")
    .then(response => response.json())
    .then(data => {
      console.log(data)
      if(data.status){
        this.setState({proposals:data.data,loading:false})
      }else{
        this.setState({loading:false,showErrorBoard:true});
      }
    }).catch(error=>{
      this.setState({loading:false,showErrorBoard:true})
    })
  }

  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <div>
            <div className="row">
              <div className="col-lg-2">
                <Link to="/vendors" className="vend-btn">
                  Inventory
                </Link>
              </div>
              <div className="col-lg-2">
                <Link className="vend-btn orange">Proposals</Link>
              </div>
              <div className="col-lg-2">
                <div className="customs-select plot-inv">
                  <select id>
                    <option value>Sort</option>
                    <option value>Default Payment</option>
                    <option value>Default Payment</option>
                    <option value>Default Payment</option>
                    <option value>Default Payment</option>
                  </select>
                  <span className="customs-arrow" />
                </div>
              </div>
              <div className="col-lg-2">
                <Link to="/vendors/add" className="vend-btn green">Add Vendor</Link>
              </div>
              <div className="col-lg-2">
                <Link to="/vendors/jobs/create" className="vend-btn black-bg">Create Job Card</Link>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="full-width mt-5 w-100">
                  <div className="pagenation">
                    <div className="left-arrow">
                      <i className="fas fa-angle-left" />
                      <i className="fas fa-angle-left" />
                    </div>
                    <div className="page-number">1-50</div>
                    <div className="right-arrow">
                      <i className="fas fa-angle-right" />
                      <i className="fas fa-angle-right" />
                    </div>
                  </div>
                  {this.state.proposals.length >0 ? 
                  <table
                    id="dtBasicExample"
                    className="table subscriber-table table-borderless table-lg w-100"
                    cellSpacing={0}
                    width="100%"
                  >
                    <thead>
                      <tr className>
                        <th className="th-sm subn">Jobs</th>
                        <th className="th-sm subn">vendor</th>
                        <th className="th-sm subn">contact</th>
                        <th className="th-sm">Jobs done</th>
                        <th className="th-sm subn">Plot Address</th>
                        <th className="th-sm subn">Ratings</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          Capentry
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          George Fields
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          Unavailable
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          1
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          8, Adele close
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group mt-n2">
                          <span id="rateMe2" className="empty-stars" />
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          Capentry
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          George Fields
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          Unavailable
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          1
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          8, Adele close
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group mt-n2">
                          <span id="rateMe2" className="empty-stars" />
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          Capentry
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          George Fields
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          Unavailable
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          1
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          8, Adele close
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group mt-n2">
                          <span id="rateMe2" className="empty-stars" />
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label htmlFor className="names-group">
                          Capentry
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          George Fields
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          Unavailable
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-info">
                          1
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group">
                          8, Adele close
                        </label>
                      </td>
                      <td>
                        <label htmlFor className="names-group mt-n2">
                          <span id="rateMe2" className="empty-stars" />
                        </label>
                      </td>
                    </tr>
                  </tbody>
                    </table>
                 : <div className="text-center"><h3>There are no proposals to display at the moment</h3></div>}
                </div>
              </div>
            </div>
          </div>
        </BodyTemplate>
      </>
    );
  }
}

export default Proposals;
