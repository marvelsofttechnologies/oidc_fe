import React, { Component } from "react";
import BodyTemplate from "../../utilities/BodyTemplate";

class AddJobCard extends Component {
  state = {
    loading: false,
    showErrorBoard: false,
  };
  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <div className="row">
            <div className="col-lg-10">
              <input
                type="text"
                className="job-input"
                placeholder="Address"
                name="address"
              />
              <input
                type="text"
                className="job-input"
                placeholder="Business Type"
                name="business"
              />
              <input
                type="text"
                className="job-input"
                placeholder="Nature of work"
                name="nature"
              />
              <textarea
                name
                id
                cols={30}
                rows={6}
                className="job-input"
                defaultValue={""}
              />
              <div className="d-flex mt-4">
                <div className="job-btn">
                  <a href="#">Create Job Card</a>
                </div>
              </div>
            </div>
          </div>
        </BodyTemplate>
      </>
    );
  }
}

export default AddJobCard;
