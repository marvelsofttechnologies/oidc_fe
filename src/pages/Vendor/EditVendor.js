import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";

class EditVendor extends Component {
  state = {
    vendorId: 0,
    loading: false,
    showErrorBoard: false,
  };
  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    var id = params.id;
    this.setState({
      vendorId: id,
    });
  }
  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <form action>
            <div className="row">
              <div className="col-lg-10">
                <div className="signup-full">
                  <div className="joint-input">
                    <input
                      type="text"
                      placeholder="Surname"
                      className="joint"
                    />
                    <input
                      type="text"
                      placeholder="Lastname"
                      className="joint"
                    />
                  </div>
                  <input
                    type="text"
                    placeholder="RC Number"
                    className="signinput-field"
                  />
                  <input
                    type="text"
                    placeholder="Office Address"
                    className="signinput-field"
                  />
                  <div id="phonenumber">
                    <input
                      type="text"
                      placeholder="Type of business"
                      className="signinput-field"
                    />
                    <div className="plus" id="tel" />
                  </div>
                  <div id="phonenumber">
                    <input
                      type="tel"
                      placeholder="Telephone Number"
                      className="signinput-field"
                      required
                    />
                    <div className="plus" id="tel" />
                  </div>
                  <div id="phonenumber">
                    <input
                      type="tel"
                      placeholder="Web Address"
                      className="signinput-field"
                      required
                    />
                    <div className="plus" id="tel" />
                  </div>
                  <div className="kin">
                    <h4>Contact Person</h4>
                    <div className="joint-input">
                      <input
                        type="text"
                        placeholder="Surname"
                        className="joint"
                      />
                      <input
                        type="text"
                        placeholder="Lastname"
                        className="joint"
                      />
                    </div>
                    <div id="phonenumber">
                      <input
                        type="tel"
                        placeholder="Telephone Number"
                        className="signinput-field"
                        required
                      />
                      <div className="plus" id="tel" />
                    </div>
                    <input
                      type="text"
                      placeholder="Mailing Address"
                      className="signinput-field"
                    />
                  </div>
                </div>
                <div className="checks mt7">
                  <button type="submit" className="btn-reg ml-n6 orangee">
                    Add
                  </button>
                </div>
              </div>
            </div>
          </form>
        </BodyTemplate>
      </>
    );
  }
}

export default EditVendor;
