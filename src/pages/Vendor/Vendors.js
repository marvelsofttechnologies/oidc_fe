import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import {Link} from 'react-router-dom';
import BodyTemplate from '../../utilities/BodyTemplate';
import Fetch from "../../utilities/Fetch";

class Vendors extends Component {
  state = {
    showErrorBoard:false,
    loading: false,
    vendors:[]
  }

  componentDidMount(){
    this.listVendors();
  }
  listVendors = () => {
    this.setState({loading:true});
    Fetch("admin/vendors","get")
    .then(response => response.json())
    .then(data => {
      console.log(data);
      if(data.status){
        this.setState({vendors:data.data,loading:false})
      }else{
        this.setState({loading:false,showErrorBoard:true})
      }
    }).catch(error => {
      console.log(error);
      this.setState({loading:false,showErrorBoard:true});
    })
  }

  render() {
    return (
      <>
      <BodyTemplate loading={this.
state.loading} showErrorBoard={this.state.showErrorBoard}>
      <div class="row">
            <div class="col-lg-2"> <Link class="vend-btn orange">Inventory</Link></div>
            <div class="col-lg-2"> <Link to="/proposals" class="vend-btn">Proposals</Link></div>
            <div class="col-lg-2">
             <div class="customs-select plot-inv">
                 <select id="">
                     <option value="">Sort</option>
                     <option value="">Default Payment</option>
                     <option value="">Default Payment</option>
                     <option value="">Default Payment</option>
                     <option value="">Default Payment</option>
                 </select>
                 <span class="customs-arrow"></span>
             </div>
            </div>
            <div class="col-lg-2"> <Link to="/vendors/add" class="vend-btn green">Add Vendor</Link></div>
            <div class="col-lg-2"> <Link to="/vendors/jobs/create" class="vend-btn black-bg">Create Job Card</Link></div>
        </div>
        <div class="row">
           <div class="col-lg-10">
            <div class="full-width mt-5 w-100">
                <div class="pagenation">
                    <div class="left-arrow">
                        <i class="fas fa-angle-left"></i>
                        <i class="fas fa-angle-left"></i>
                    </div>
                    <div class="page-number">1-50</div>
                    <div class="right-arrow">
                        <i class="fas fa-angle-right"></i>
                        <i class="fas fa-angle-right"></i>
                    </div>
                </div>
                <table id="dtBasicExample" class="table subscriber-table table-borderless table-lg w-100" cellspacing="0" width="100%">
                   <thead>
                       <tr class="">
                        <th class="th-sm subn">Firstname</th>
                        <th class="th-sm subn">Last Name</th>
                        <th class="th-sm">Middle Name</th>
                        <th class="th-sm">Email</th>
                        <th class="th-sm">Phone Number</th>
                       </tr>
                   </thead>
                   <tbody>
                     {this.state.vendors.map((vendor,index) => {
                       return<tr>
                       <td>
                         <label for="" class="names-group">{vendor.firstName}</label>
                           
                       </td>
                   
                       <td>
                           <label for="" class="names-group">{vendor.lastName}</label>
                       </td>
                       <td>
                         <label for="" class="names-info">{vendor.middleName}</label>
                       </td>
                       <td>
                         <label for="" class="names-info">{vendor.email}</label>
                       </td>

                       <td>
                       <label for="" class="names-info">{vendor.phoneNumber}</label>
                       </td>
                   </tr>
                   
                     })}
                       
                      
                       
                   </tbody>
               </table>
            </div> 
           </div>
       </div>
      </BodyTemplate>
      </>
    );
  }
}

export default Vendors;
