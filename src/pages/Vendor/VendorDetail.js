import React, { Component } from "react";
import BodyTemplate from "../../utilities/BodyTemplate";
import { Link } from "react-router-dom";

class VendorDetail extends Component {
  state = {
    loading: false,
    showErrorBoard: false,
  };
  render() {
    return (
      <>
        <BodyTemplate
          loading={this.state.loading}
          showErrorBoard={this.state.showErrorBoard}
        >
          <div>
            <div className="row">
              <div className="col-lg-9">
                <div className="row">
                  <div className="col-lg-3">
                    <div className="imgs-circle">
                      <img src="/Assets/Avatar.png" alt />
                    </div>
                  </div>
                  <div className="col mt8">
                    <h2 className="user-name">
                      George Fields <span>(Mason)</span>
                    </h2>
                    <div className="row">
                      <div className="col">
                        <h6 className="proposals mb-2">
                          Email: <span> george.fields@gmail.com</span>
                        </h6>
                        <h6 className="proposals">
                          Phone Number: <span>0811 234 5678</span>
                        </h6>
                      </div>
                      <div className="col">
                        <h6 className="proposals mb-2">
                          Website: <span> georgefields.com</span>
                        </h6>
                        <h6 className="proposals">
                          RC Number: <span> 01-12345 </span>
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col" />
              <div className="col-lg-2">
                <Link to="/vendor/edit/1" className="res-btn borderly orangee">
                  Edit Vendor Detail
                </Link>
                <a href className="res-btn borderly black">
                  Delete Vendor
                </a>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-9">
                <h4 className="plots mb-4 ">Work History</h4>
                <div className="full-width mt-2 w-100">
                  {/* <div class="pagenation">
                      <div class="left-arrow">
                          <i class="fas fa-angle-left"></i>
                          <i class="fas fa-angle-left"></i>
                      </div>
                      <div class="page-number">1-50</div>
                      <div class="right-arrow">
                          <i class="fas fa-angle-right"></i>
                          <i class="fas fa-angle-right"></i>
                      </div>
                  </div> */}
                  <table
                    id="dtBasicExample"
                    className="table subscriber-table table-borderless table-lg w-100"
                    cellSpacing={0}
                    width="100%"
                  >
                    <thead>
                      <tr className>
                        <th className="th-sm subn">vendor</th>
                        <th className="th-sm subn">Jobs done</th>
                        <th className="th-sm subn">Address</th>
                        <th className="th-sm subn">Ratings</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <label htmlFor className="names-group">
                            George Fields
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            Capentry
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            8, Adele close
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group mt-n2">
                            <span id="rateMe2" className="empty-stars" />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label htmlFor className="names-group">
                            George Fields
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            Capentry
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            8, Adele close
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group mt-n2">
                            <span id="rateMe2" className="empty-stars" />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label htmlFor className="names-group">
                            George Fields
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            Capentry
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            8, Adele close
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group mt-n2">
                            <span id="rateMe2" className="empty-stars" />
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label htmlFor className="names-group">
                            George Fields
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            Capentry
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group">
                            8, Adele close
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-group mt-n2">
                            <span id="rateMe2" className="empty-stars" />
                          </label>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </BodyTemplate>
      </>
    );
  }
}

export default VendorDetail;
