import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import BodyTemplate from "../../utilities/BodyTemplate";
import ErrorBoard from "../../utilities/ErrorBoard";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";
import {Link} from 'react-router-dom';

class SubscriberDetails extends Component {
  state = {
    loading: false,
    subscriber: {},
    kin:{},
    payments:[],
    plots:[],
    requests:[],
    interestedPlots:[],
    subscriberId: 0,
    showErrorBoard:false
  };

  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    let id = params.id;
    console.log(id);
    this.setState({ subscriberId: id }, () => this.getDetails());
    //this.getDetails();
  }

  getDetails = async () => {
    try {
      this.setState({ loading: true });
      let response = await Fetch(
        `admin/subscriber/${this.state.subscriberId}`,
        "get"
      );
      let data = await response.json();
      if(data.status){
        this.setState({ 
          loading: false,
          subscriber:data.data,
          kin:data.data.nextOfKin,
          plots:data.data.plots,
          payments:data.data.payments,
          interestedPlots:data.data.interestedPlots,
          requests:data.data.requests
         });
        console.log(data.data);
      }else{
        this.setState({loading:false,showErrorBoard:true})
      }
    }catch(error){
      console.log(error);
      this.setState({loading:false,showErrorBoard:true})
    }
  };
  render() {
    return (
     <BodyTemplate loading={this.state.loading} showErrorBoard={this.state.showErrorBoard}>
      <div className="row">
  <div className="col-lg-6">
    <div className="row">
      <div className="col-lg-12">
        <h4 className="sub-details">{this.state.subscriber.fullName}</h4>
        <h4 className="sub-details">{this.state.subscriber.residentialAddress}</h4>
        <h4 className="sub-details">{this.state.subscriber.mailingAddress}</h4>
        <div className="joints">
          <h4 className="sub-details">{this.state.subscriber.phoneNumber}</h4>
          {/* <h4 className="sub-details">Telephone Number 1</h4> */}
        </div>
      </div>
      <div className="col-lg-12">
        <div className="kin">
          <h4>Next of Kin</h4>
        </div>
        <h4 className="sub-details">{this.state.kin.firstName + " " + this.state.kin.lastName}</h4>
        <h4 className="sub-details">{this.state.kin.phoneNumber}</h4>
        <h4 className="sub-details">{this.state.kin.address}</h4>
      </div>
    </div>
  </div>
  <div className="col-lg-2">
    <h5 className="user-id">User ID:         <span>{this.state.subscriber.userId}</span></h5>
  </div>
  <div className="col-lg-4">
    <div className="subscriber-image">
      <img src={this.state.subscriber.photoUrl} alt />
    </div>
  </div>
  <div className="col-lg-12">
    <div className="dividers" /> 
  </div>
  <div className="col-lg-6">
    <div className="kin">
      <h4>Plot(s)</h4>
    </div>
    <table className="table table-borderless" width="100%">
      <thead>
        <tr className="top">
          <th className="th-sm subn">Plots owned</th>
          <th className="th-sm subn">Type</th>
          <th className="th-sm subn">Payment</th>
        </tr>
      </thead><tbody>
        
      {this.state.plots  ?
      this.state.plots.map((plot,index) => {
          return <tr>
          <Link to={`/plots/${plot.plotId}`}><td><label htmlFor className="names-group">{plot.plotName}</label></td></Link>
          <td><label htmlFor className="names-group">{plot.plotType}</label></td>
          <td><label htmlFor className="names-group">{plot.isPaymentComplete ? "Complete" : "Incomplete"}</label></td>   
        </tr>
        
        }) : <span>This user has no plots</span> }
        </tbody>
    </table>
    <div className="kin">
      <h4>Plot(s)</h4>
    </div>
    <table className="table table-borderless" width="100%">
      <thead>
        <tr className="top">
          <th className="th-sm subn pl-3">Plot interested in</th>
          <th className="th-sm">Type</th>
        </tr>
      </thead><tbody>
      {this.state.interestedPlots  ?
      this.state.interestedPlots.map((plot,index) => {
          return <tr>
          <Link to={`/plots/${plot.plotId}`}><td><label htmlFor className="names-group">{plot.plotName}</label></td></Link>
          <td><label htmlFor className="names-group">{plot.plotType}</label></td>  
        </tr>
        
        }) : <span>This user has no interested plots</span> }
        </tbody>
    </table>
    <div className="dividers" />
    <div className="kin">
      <h4>Payment Timeline</h4>
    </div>
    <div className="pay-title">Payment history</div>
    <div className="surround-table mt-3 mb-4">
      <table className="table table-borderless" width="100%">
        <thead>
          <tr>
            <th className="th-xsm subn">Date </th>
            <th className="th-lg subn">Details</th>
            <th className="th-sm">Plot</th>
            <th className="th-sm subn">Price</th>
          </tr>
        </thead><tbody>
          {this.state.payments ?
          this.state.payments.map((payment,index) => {
            return <tr>
            <td><label htmlFor className="names-group">{new Date(payment.paymentDate).toLocaleDateString()}</label></td>
            <td><label htmlFor className="names-group">{payment.paymentType}</label></td>
            <td><label htmlFor className="names-info">{payment.plotId}</label></td>
            <td><label htmlFor className="names-group">{payment.amount}</label></td>
          </tr>
          })
        : null}
        </tbody>
      </table>
    </div>
    {/* <div className="pay-title">Payment update</div>
    <div className="surround-table mt-3 mb-4">
      <table className="table table-borderless" width="100%">
        <thead>
          <tr>
            <th className="th-xsm subn">Date </th>
            <th className="th-lg subn">Details</th>
            <th className="th-sm">Plot</th>
            <th className="th-sm subn">Price</th>
          </tr>
        </thead><tbody>
          <tr>
            <td><label htmlFor className="names-group red-text">12/05/20</label></td>
            <td><label htmlFor className="names-group red-text">First payment</label></td>
            <td><label htmlFor className="names-info red-text">1</label></td>
            <td><label htmlFor className="names-group red-text">1,000,000.00</label></td>
          </tr>
          <tr>
            <td><label htmlFor className="names-group">12/05/20</label></td>
            <td><label htmlFor className="names-group">First payment</label></td>
            <td><label htmlFor className="names-info">1</label></td>
            <td><label htmlFor className="names-group">1,000,000.00</label></td>
          </tr>
          <tr>
            <td><label htmlFor className="names-group red-text">12/05/20</label></td>
            <td><label htmlFor className="names-group red-text">First payment</label></td>
            <td><label htmlFor className="names-info red-text">1</label></td>
            <td><label htmlFor className="names-group red-text">1,000,000.00</label></td>
          </tr>
        </tbody>
      </table>
    </div> */}
  </div>
  <div className="col-lg-6">
  {/* <div className="kin">
      <h4>Message(s)</h4>
    </div>
    <div className="inbox-tit">
      <div className="inbox-time justify-content-center">Time</div>
      <div className="inbox-time w-95">Description</div>
    </div>
    <div className="inbox-tit">
      <div className="inbox-msg-time greeen">08.00am</div>
      <div className="inbox-msg-body greeen">
        <h5>Message TiTle</h5>
        <p>Message body</p>
      </div>
    </div>
    <div className="inbox-tit">
      <div className="inbox-msg-time">08.00am</div>
      <div className="inbox-msg-body">
        <h5>Message TiTle</h5>
        <p>Message body</p>
      </div>
    </div>
    <div className="dividers" /> */}
    {/* <button className="inbox-send">Send a message</button> */}
    <div className="kin">
      <h4>Send a Message</h4>
    </div>
    <textarea name="message subject" id rows={2} className="msg-areas mb-0" placeholder="Message subject" defaultValue={""} />
    <textarea name="message-details" id rows={10} className="msg-area mt-n3" placeholder="Message details" defaultValue={""} />
    <div className="button-div">
      <button className="bt-div" style={{backgroundColor:"red", color:'black'}}>CANCEL MESSAGE</button>
      <button className="bt-div" style={{backgroundColor:"#327C47 ", color:'white'}}>Send MESSAGE</button>
    </div>
    {/* <button className="add-sub-btn mt-3 w-25 mx-auto">Back</button> */}
    <div className="dividers" />
    <div className="kin">
      <h4>Request(s)</h4>
    </div>
    <div className="pay-title">Requests</div>
    <div className="surround-table mt-3 mb-4">
      <table className="table table-borderless" width="100%">
        <thead>
          <tr>
            <th className="th-xsm subn">Date </th>
            <th className="th-lg subn">Request Type</th>
            <th className="th-sm">Plot</th>
            <th className="th-sm subn">Status</th>
          </tr>
        </thead><tbody>
          {this.state.requests ?
          this.state.requests.map((request,index) => {
            return <tr>
            <td><label htmlFor className="names-group">{new Date(request.requestDate).toLocaleDateString()}</label></td>
            <td><label htmlFor className="names-group">{request.requestType}</label></td>
            <td><label htmlFor className="names-info">{request.plotId}</label></td>
            <td><label htmlFor className="names-group">{request.requestStatus}</label></td>
          </tr>
          })
        : 
        <div class="text-center"><span>This user has no requests</span></div>
        }
        </tbody>
      </table>
    </div>
  </div>
  
</div>

      </BodyTemplate>
    );
  }
}

export default SubscriberDetails;
