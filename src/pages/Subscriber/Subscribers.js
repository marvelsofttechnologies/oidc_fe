import React, { Component } from "react";
import SideNav from "../../components/Navigations/SideNav";
import { MainContext } from "../../contexts/MainContext";
import { Link } from "react-router-dom";
import Spinner from "../../utilities/Spinner";
import Fetch from "../../utilities/Fetch";
import BodyTemplate from "../../utilities/BodyTemplate";
import Modal from "../../utilities/Modal";

class Subscribers extends Component {
  static contextType = MainContext;
  state = {
    loading: false,
    allSubscribers:[],
    subscribers: [],
    errorMessage: "",
    showErrorBoard: false,
    paymentSortOption: "",
    plotSortOption: "",
    userToDelete:0,
    reloading:false,
    sortOption:''
  };

  listSubscriber = () => {
    this.setState({ loading: true });
    Fetch("metrics/subscribers/existing", "GET")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          this.setState({ subscribers: data.data,allSubscribers:data.data });
          this.setState({ loading: false });
        } else {
          this.setState({ errorMessage: data.errors[0] });
          this.setState({ loading: false });
        }
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
      });
  };

  handleChanges = (e) => {
    console.log("amy");
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value }, () => this.sortSubscribers());
  };

  sortSubscribers = () => {
    let sortOption = this.state.sortOption;
    console.log(this.state);
    let sortedSubscribers = [];
    if(sortOption == "Has a Plot"){
      sortedSubscribers = this.state.allSubscribers.filter(subscriber => subscriber.plots.length > 0);
      console.log(sortedSubscribers);
      this.setState({subscribers:sortedSubscribers});
    }
    if(sortOption == "Has no Plot"){
      sortedSubscribers = this.state.allSubscribers.filter(subscriber => subscriber.plots.length < 1);
      this.setState({subscribers:sortedSubscribers});
    }
    if(sortOption == 'incomplete'){
      sortedSubscribers = this.state.allSubscribers.filter(subscriber => subscriber.paymentPercentage < '100');
      this.setState({subscribers:sortedSubscribers});
    }
    if(sortOption == 'complete'){
      sortedSubscribers = this.state.allSubscribers.filter(subscriber => subscriber.paymentPercentage == '100');
      console.log(sortedSubscribers);
      this.setState({subscribers:sortedSubscribers});
    }
    if(sortOption == 'All'){
      this.setState({subscribers:this.state
      .allSubscribers});
    }
  };
  setDeletingUserId = (id) =>{
    this.setState({userToDelete:id});
  }

  deleteSubscriber = async () => {
    alert('deleting user with id ' + this.state.userToDelete)
  }

  reload = () => {
    this.setState({reloading: true}, ()=>{
      setTimeout(() => {
          this.setState({reloading: false});
      }, 1000);
  })
  }

  cancelDelete = () =>{
    this.setState({userToDelete:0});
    this.reload();
  }

  componentDidMount() {
    this.listSubscriber();
  }
  render() {
    return (
      <BodyTemplate
        loading={this.state.loading}
        showErrorBoard={this.state.showErrorBoard}
      >
        
        {this.state.reloading ? null : 
        <Modal id={"DeleteUser"} title={"Confirm Delete"} unloadCallback={this.reload}>
        <div class="del-notification">
                            <div class="text-icon d-flex ml-2">
                             <i class="fas fa-trash"></i>
                             <h4>Are you sure you want to delete this subscriber?</h4>
                            </div>
                               <div class="butns d-flex">
                                <button onClick={() => this.cancelDelete()} class="no edit-btn">No</button>
                                <button onClick={() => this.deleteSubscriber()} class="dl edit-btn">Delete</button>
                               </div>
                           </div>
        </Modal>
        }
        {this.state.subscribers.length > 0 ? (
          <>
            <div className="row">
              <div className="col-lg-3">
                <div className="customs-select sub">
                  <select
                    name="sortOption"
                    onChange={this.handleChanges}
                  >
                    <option value="All">All</option>
                    <option value="incomplete">Incomplete Payment</option>
                    <option value="complete">Completeed Payment </option>
                  </select>
                  <span className="customs-arrow" />
                </div>
              </div>
              <div className="col-lg-3">
                <div className="customs-select sub">
                  <select name="sortOption" onChange={this.handleChanges}>
                    <option value="All">All</option>
                    <option value="Has a Plot">Has a Plot</option>
                    <option value="Has no Plot">Has no Plot</option>
                  </select>
                  <span className="customs-arrow" />
                </div>
              </div>
              <div className="col-lg-2">
                <Link to="/subscribers/add" className="add-sub-btn">
                  Add Subscriber
                </Link>
              </div>
            </div>

            <div className="full-width mt-5">
              <table
                id="dtBasicExample"
                className="table subscriber-table table-borderless table-lg "
                cellSpacing={0}
                width="100%"
              >
                <thead>
                  <tr className>
                    <th className="th-sm subn">Subscriber </th>
                    <th className="th-sm">Phone Number</th>
                    <th className="th-sm">Email</th>
                    <th className="th-sm">Number of plots</th>
                    <th className="th-sm">Payment Update</th>
                    <th className="th-sm">Edit</th>
                    <th className="th-sm">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.subscribers.map((subscriber, index) => {
                    return (
                      <tr>
                        <Link to={`subscribers/${subscriber.userId}`}>
                          <td>
                            <label className="names-group">
                              {/* <img
                                    src={subscriber.photoUrl}
                                    alt
                                    className="img-circle lbl"
                                  /> */}
                              <Link to={`subscribers/${subscriber.userId}`}>
                                {subscriber.fullName}
                              </Link>
                            </label>
                          </td>
                        </Link>
                        <td>
                          <label htmlFor className="names-info">
                            {subscriber.phoneNumber}
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-info">
                            {subscriber.email}
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-info">
                            {subscriber.plots.length}
                          </label>
                        </td>
                        <td>
                          <label htmlFor className="names-info bars">
                            <div className="progress">
                              <div
                                className="progress-bar"
                                role="progressbar"
                                style={{
                                  width:
                                    subscriber.paymentPercentage > "0"
                                      ? `${subscriber.paymentPercentage.toString()}%`
                                      : "100",
                                }}
                                aria-valuenow={0}
                                aria-valuemin={0}
                                aria-valuemax={100}
                                {...(subscriber.paymentPercentage == "0" ||
                                subscriber.paymentPercentage == 0
                                  ? ""
                                  : null)}
                              >
                                {`${subscriber.paymentPercentage}%`}
                              </div>
                            </div>
                          </label>
                        </td>
                        <td>
                          <Link to={`/subscribers/edit/${subscriber.userId}`}>
                            {" "}
                            <i className="fas fa-align-center de" />
                          </Link>
                        </td>
                        <td>
                          <i
                            className="fas fa-trash de"
                            data-toggle="modal"
                            data-target="#DeleteUser"
                            onClick={() => this.setDeletingUserId(subscriber.userId)}
                          />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </>
        ) : (
          <>
            <div className="text-center mt-5 ">
              <i
                class="fas fa-filter m-3"
                style={{ fontSize: "30px", color: "black" }}
              ></i>
              <h3>There are currently no Subscribers to display</h3>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <Link to="/subscribers/add" className="add-sub-btn mt-3">
                  Add Subscriber
                </Link>
              </div>
            </div>
          </>
        )}
        
      </BodyTemplate>
    );
  }
}

export default Subscribers;
