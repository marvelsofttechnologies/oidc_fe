import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import Alert from "../../utilities/Alert";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";
import BodyTemplate from '../../utilities/BodyTemplate';
class AddSubscriber extends Component {
  static contextType = MainContext;
  state = {
    isIndividual: true,
    isCorparate: false,
    agreed: false,
    surname: "",
    lastname: "",
    emailAddress: "",
    residentialAddress: "",
    mailingAddress: "",
    phoneNumber: "",
    kinSurname: "",
    kinLastname: "",
    kinMailingAddress: "",
    kinPhoneNumber: "",
    nameOfEntry: "",
    RCNumber: "",
    typeOfBusiness: "",
    officeAddress: "",
    TelephoneNumber: "",
    authSurname: "",
    authLastname: "",
    authAddress: "",
    authphoneNumber: "",
    submiting:false
  };

  toggleForm = () => {
    this.setState({
      isIndividual: !this.state.isIndividual,
      isCorparate: !this.state.isCorparate,
    });
  };

  componentDidMount() {}

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  addIndividual = (e) => {
    const context = this.context;
    this.setState({ submiting: true });
    let newIndividualSubscriber = {
      firstName: this.state.surname,
      lastName: this.state.lastname,
      gender: "",
      email: this.state.emailAddress,
      phoneNumber: this.state.phoneNumber,
      residentialAddress: this.state.residentialAddress,
      mailingAddress: this.state.mailingAddress,
      stateOfOriginId: 0,
      userTypeId: 0,
      nextOfKin: {
        firstName: this.state.kinLastname,
        lastName: this.state.kinSurname,
        phoneNumber: this.state.kinPhoneNumber,
        address: this.state.kinMailingAddress,
      },
    };
    Fetch("admin/subscriber/individual", "POST", newIndividualSubscriber)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.status) {
          context.showAlert(
            "success",
            "New Subscriber added successfully",
            "Success"
          );
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
          window.location.href = '/subscribers'
          this.setState({ submiting: false });
        } else {
          context.showAlert(
            "danger",
            data.message.toString(),
            data.message.toString()
          );
          console.log(data);
          this.setState({ submiting: false });
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ submiting: false });
      });
    console.log(newIndividualSubscriber);
    e.preventDefault();
  };

  addCorporate = (e) => {
    const context = this.context;
    this.setState({ submiting: true });
    let newCorporate = {
      nameOfEntry: this.state.nameOfEntry,
      email: this.state.emailAddress,
      officeAddress: this.state.officeAddress,
      mailingAddress: this.state.mailingAddress,
      stateOfOriginId: 0,
      phoneNumber: this.state.TelephoneNumber,
      platform: "web",
      profilePhoto: "",
      webSiteUrl: "",
      rcNumber: this.state.rcNumber,
    };
    Fetch("admin/subscriber/corporate", "POST", newCorporate)
    .then((res) => res.json())
    .then((data) => {
      if (data.status) {
        context.showAlert(
          "success",
          "New Subscriber added successfully",
          "Success"
        );
        console.log(data);
        
        this.setState({ submiting: false });
        window.scroll({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
        window.location.href = '/subscribers'
          this.setState({ submiting: false });
      } else {
        context.showAlert(
          "danger",
          data.message.toString(),
          data.message.toString()
        );
        
        this.setState({ submiting: false });
        window.scroll({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
        
      }
    })
    .catch((error) => {
      console.log(error);
      this.setState({ submiting: false });
    });
    console.log(newCorporate);
    e.preventDefault();
  };

  render() {
    return (
<BodyTemplate loading={this.state.loading} showErrorBoard={this.state.showErrorBoard}>
                <Alert/>
                <div className="row">
                  <div className="col-lg-5">
                    <div className="check-body">
                      <h5>Category</h5>
                      <div className="new-check d-flex">
                        <div className="checks">
                          <input
                            type="checkbox"
                            id="ind"
                            onChange={this.toggleForm}
                            checked={this.state.isIndividual ? true : false}
                          />{" "}
                          <label htmlFor="ind">Individual</label>
                        </div>
                        <div className="checks">
                          <input
                            type="checkbox"
                            id="cop"
                            onChange={this.toggleForm}
                            checked={this.state.isCorparate ? true : false}
                          />
                          <label htmlFor="cop">Corporate</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {this.state.isIndividual ? (
                  <form onSubmit={this.addIndividual}>
                    <div className="row">
                      <div className="col-lg-10">
                        <div className="signup-full">
                          <div className="joint-input">
                            <input
                              onChange={this.handleChanges}
                              type="text"
                              placeholder="Surname"
                              name="surname"
                              className="joint"
                              required
                            />
                            <input
                              onChange={this.handleChanges}
                              type="text"
                              placeholder="Lastname"
                              name="lastname"
                              className="joint"
                              required
                            />
                          </div>
                          <input
                            onChange={this.handleChanges}
                            type="email"
                            placeholder="Email Address"
                            name="emailAddress"
                            className="signinput-field"
                            required
                          />
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="Residential Address"
                            name="residentialAddress"
                            className="signinput-field"
                            required
                          />
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="Mailing Address"
                            name="mailingAddress"
                            className="signinput-field"
                            required
                          />
                          <div id="phonenumber">
                            <input
                              onChange={this.handleChanges}
                              type="tel"
                              placeholder="Telephone Number"
                              name="phoneNumber"
                              className="signinput-field"
                              required
                            />
                            <div className="plus" id="tel" />
                          </div>
                          <div className="kin">
                            <h4>Next of Kin</h4>
                            <div className="joint-input">
                              <input
                                onChange={this.handleChanges}
                                type="text"
                                placeholder="Surname"
                                name="kinSurname"
                                className="joint"
                                required
                              />
                              <input
                                onChange={this.handleChanges}
                                type="text"
                                placeholder="Lastname"
                                name="kinLastname"
                                className="joint"
                                required
                              />
                            </div>
                            <div id="phonenumber">
                              <input
                                onChange={this.handleChanges}
                                type="tel"
                                placeholder="Telephone Number"
                                name="kinPhoneNumber"
                                className="signinput-field"
                                required
                              />
                              <div className="plus" id="tel" />
                            </div>
                            <input
                              onChange={this.handleChanges}
                              type="text"
                              placeholder="Mailing Address"
                              name="kinMailingAddress"
                              className="signinput-field"
                              required
                            />
                          </div>
                        </div>
                        <div className="checks mt7">
                          <input
                            name="agreed"
                            onChange={this.handleChanges}
                            type="checkbox"
                            id="cap"
                            required
                          />
                          <label htmlFor="cap">
                            I have read the terms and conditions
                          </label>
                          <button
                            type="submit"
                            className={
                              this.state.agreed
                                ? "btn-reg ml-n6 checked"
                                : "btn-reg ml-n6 disabled"
                            }
                          >
                            {this.state.submiting ? <Spinner /> : "Add"}
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                ) : (
                  <form onSubmit={this.addCorporate}>
                    <div className="row">
                      <div className="col-lg-10">
                        <div className="signup-full">
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="Name of entry"
                            name="nameOfEntry"
                            className="signinput-field"
                          />
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="RC Number"
                            name="rcNumber"
                            className="signinput-field"
                          />
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="Office address"
                            name="officeAddress"
                            className="signinput-field"
                          />
                          <input
                              onChange={this.handleChanges}
                              type="text"
                              placeholder="Mailing Address"
                              name="mailingAddress"
                              className="signinput-field"
                              required
                            />
                          <input
                            onChange={this.handleChanges}
                            type="text"
                            placeholder="Type of business"
                            name="typeOfBusiness"
                            className="signinput-field"
                          />
                          <div id="phonenumber">
                            <input
                              onChange={this.handleChanges}
                              type="tel"
                              placeholder="Telephone Number"
                              name="TelephoneNumber"
                              className="signinput-field"
                              required
                            />
                            <div className="plus" id="tel" />
                          </div>
                          <input
                            onChange={this.handleChanges}
                            type="email"
                            placeholder="Email Address"
                            name="emailAddress"
                            className="signinput-field"
                            required
                          />
                          <div className="kin">
                            <h4>Authorizing Officer</h4>
                            <div className="joint-input">
                              <input
                                onChange={this.handleChanges}
                                type="text"
                                placeholder="Surname"
                                name="authSurname"
                                className="joint"
                              />
                              <input
                                onChange={this.handleChanges}
                                type="text"
                                placeholder="Lastname"
                                name="authLastname"
                                className="joint"
                              />
                            </div>
                            <div id="phonenumber">
                              <input
                                onChange={this.handleChanges}
                                type="tel"
                                placeholder="Telephone Number"
                                name="authPhoneNumber"
                                className="signinput-field"
                                required
                              />
                              <div className="plus" id="tel" />
                            </div>
                            <input
                              onChange={this.handleChanges}
                              type="text"
                              placeholder="Address"
                              name="authAddress"
                              className="signinput-field"
                            />
                          </div>
                        </div>
                        <div className="checks mt7">
                          <input
                            name="agreed"
                            onChange={this.handleChanges}
                            type="checkbox"
                            id="cap"
                          />
                          <label htmlFor="cap">
                            I have read the terms and conditions
                          </label>
                          <button
                            type="submit"
                            className={
                              this.state.agreed
                                ? "btn-reg ml-n6 checked"
                                : "btn-reg ml-n6 disabled"
                            }
                          >
                            {this.state.submiting ? <Spinner /> : "Add"}
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                )}
     </BodyTemplate>
    );
  }
}

export default AddSubscriber;
