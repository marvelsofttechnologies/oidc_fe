import React, {useState,useContext,createContext} from 'react';

export const UserContext = createContext();

export const UserProvider = (props) => {
    const [user,setUser] = useState({});
    const value = { user, setUser };
    return (
        <UserContext.Provider value={value}>
            {props.children}
        </UserContext.Provider>
    );
};
