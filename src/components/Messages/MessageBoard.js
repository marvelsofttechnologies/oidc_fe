import React, { Component } from "react";
import Fetch from "../../utilities/Fetch";

class MessageBoard extends Component {
    state={
        messages:[],
        allMessages:[],
    }

    componentDidMount(){
        this.loadMessages();
    }

    loadMessages = async () => {
        try{
            let response = await Fetch('messages','Get');
            let data = await response.json();
            console.log(data);
            if(data.status){
                let messages =[];
                this.setState({allMessages:data.data});
                let reversedMessages = data.data.reverse();
                if(reversedMessages.lenth > 0){
                    for(let i =0;i <= 2;i++){
                        messages.push(reversedMessages[i]);
                    }
                }
                this.setState({messages:messages});
            }
        }catch(error){
            console.log(error);
        }
    }
  render() {
    return (
      <>
        <div className="msg-card">
          <div className="msg">
            <h3 className>Messages</h3>
            <input type="search" placeholder="Search" className="msg-search" />
            <svg
              width={20}
              height={21}
              viewBox="0 0 20 21"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M12.9499 13.705C13.0651 13.583 13.2019 13.4862 13.3524 13.4201C13.503 13.3541 13.6644 13.3201 13.8273 13.3201C13.9903 13.3201 14.1517 13.3541 14.3023 13.4201C14.4528 13.4862 14.5896 13.583 14.7048 13.705L19.4796 18.7581C19.7123 19.0042 19.8431 19.3381 19.8432 19.6863C19.8433 20.0344 19.7128 20.3684 19.4802 20.6147C19.2477 20.8609 18.9322 20.9994 18.6032 20.9995C18.2742 20.9996 17.9587 20.8614 17.7259 20.6153L12.9511 15.5622C12.8358 15.4403 12.7444 15.2955 12.6819 15.1362C12.6195 14.9769 12.5874 14.8061 12.5874 14.6336C12.5874 14.4611 12.6195 14.2903 12.6819 14.131C12.7444 13.9717 12.8358 13.8269 12.9511 13.705H12.9499Z"
                fill="black"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M8.06136 15.75C8.95712 15.75 9.84412 15.5633 10.6717 15.2005C11.4993 14.8377 12.2512 14.306 12.8846 13.6357C13.518 12.9654 14.0205 12.1696 14.3633 11.2937C14.7061 10.4179 14.8825 9.47923 14.8825 8.53125C14.8825 7.58327 14.7061 6.64457 14.3633 5.76875C14.0205 4.89294 13.518 4.09715 12.8846 3.42682C12.2512 2.7565 11.4993 2.22477 10.6717 1.86199C9.84412 1.49922 8.95712 1.3125 8.06136 1.3125C6.25228 1.3125 4.51729 2.07304 3.23808 3.42682C1.95886 4.7806 1.24021 6.61672 1.24021 8.53125C1.24021 10.4458 1.95886 12.2819 3.23808 13.6357C4.51729 14.9895 6.25228 15.75 8.06136 15.75ZM16.1227 8.53125C16.1227 10.7939 15.2734 12.9638 13.7616 14.5638C12.2498 16.1637 10.1994 17.0625 8.06136 17.0625C5.92335 17.0625 3.87292 16.1637 2.36112 14.5638C0.849319 12.9638 0 10.7939 0 8.53125C0 6.26862 0.849319 4.09867 2.36112 2.49875C3.87292 0.898826 5.92335 0 8.06136 0C10.1994 0 12.2498 0.898826 13.7616 2.49875C15.2734 4.09867 16.1227 6.26862 16.1227 8.53125Z"
                fill="black"
              />
            </svg>
          </div>
          {this.state.messages.length < 1 ?
          <div className="text-center">
              <p>There are no messages to display.</p>  
          </div>
        : null}
            {this.state.messages.map((message,index)=>{
               return <div class="msg-content read d-flex">
                <div>
                    <div class="avatar mt-2">
                       
                       <h3>{message.title.charAt(0)}</h3>
                    </div>
                </div>
                
                <div class="user-msg">
                    <div class="user-name d-flex justify-content-between">
                        <h4 style={{fontSize:'15px'}}>{message.title}</h4>
                    </div>
                   
                    <p>{message.message}</p>
                    <div class="reply d-flex justify-content-between">
                        <span>3 hours ago</span> <strong>Reply</strong>
                    </div>
                </div>

            </div>
            })}
            {this.state.messages.length > 3 ?
          <div className="see-all">
            <button className="cards-btn">
              <a href="#">See all</a>
            </button>
          </div>
          :null}
        </div>
      </>
    );
  }
}

export default MessageBoard;
