import React, { Component } from "react";
import Spinner from "../../utilities/Spinner";
import Fetch from "../../utilities/Fetch";
import { Redirect } from "react-router-dom";

class SIgnUp extends Component {
  state = {
    email: "",
    password: "",
    confirmPassword: "",
    loading: "",
    errorMessage: "",
    redirect:false
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    console.log(process.env);
    this.setState({ loading: true });
    console.log(this.state);
    var data = {
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      userType: 1,
      organizationType: 1,
      platform:"web"
    };
    Fetch("auth/register", "post", data)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message == "Successful") {
          this.setState({redirect:true})
        } else {
          this.setState({ errorMessage: data.message });
        }
        this.setState({ loading: false });
      })
      .catch((error) => {
        console.error(error);
      });
    e.preventDefault();
  };

  render() {
    return this.state.redirect ? (
      <Redirect to={{pathname:"/register/verify", 
      state:{email: this.state.email} 
    }}/>
    ) : (
      <>
        <div id="sign-up-form" className={this.props.active ? "show" : ""}>
          <form onSubmit={this.handleSubmit}>
            {this.state.errorMessage ? (
              <div className="text-center mb-2">
                <span className="text-danger text-center mb-2">
                  {this.state.errorMessage}
                </span>
              </div>
            ) : null}
            <input
              name="email"
              type="email"
              placeholder="E-mail"
              className="input-field"
              onChange={this.handleChanges}
              required
            />
            <input
              name="password"
              type="password"
              placeholder="Password"
              className="input-field"
              onChange={this.handleChanges}
              required
            />
            <input
              name="confirmPassword"
              type="password"
              placeholder="Re-type Password"
              className="input-field"
              onChange={this.handleChanges}
              required
            />
            {/* <span className="cat">Category</span>
            <div className="checkbox">
              <input type="checkbox" id="ind" defaultChecked />{" "}
              <label htmlFor="ind">Individual</label>
            </div>
            <div className="checkbox">
              <input type="checkbox" id="cop" />
              <label htmlFor="cop">Corporate</label>
            </div> */}
            {/* {this.state.loading ?
              <div className="text-center">
                <Spinner size={'small'}></Spinner>
              </div>
             : */}
            <button type="submit" className="sign-btns">
              {this.state.loading ? (
                <Spinner size={"small"}></Spinner>
              ) : (
                "Sign Up"
              )}
            </button>
            {/* } */}
          </form>
        </div>
      </>
    );
  }
}

export default SIgnUp;
