import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import Fetch from "../../utilities/Fetch";
import Spinner from "../../utilities/Spinner";
import AquiredPlots from "../Charts/AquiredPlots";
import PlotAllocation from "../Charts/PlotAllocation";
import BodyTemplate from '../../utilities/BodyTemplate';
import MessageBoard from '../Messages/MessageBoard';

class Index extends Component {
  state = {
    metrics: {},
    loading: false,
    showErrorBoard:false
  };

  componentDidMount(){
    this.getMetrics();
  }
  getMetrics = () => {
    this.setState({ loading: true });
    Fetch("metrics/counts", "GET")
      .then((res) => res.json())
      .then((data) => {
        if (data.status) {
          this.setState({ metrics: data.data, loading: false });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false,showErrorBoard:true });
      });
  };
  render() {
    return (
<BodyTemplate loading={this.state.loading} showErrorBoard={this.state.showErrorBoard} >
                  <div className="row">
                    <div className="col-lg-8">
                      <div className="mini-cards">
                        <div className="row">
                          <div className="col-lg-6">
                            <div className="cards">
                              <div className="icon">
                                <svg
                                  width={50}
                                  height={50}
                                  viewBox="0 0 50 50"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <circle cx={25} cy={25} r={25} fill="black" />
                                  <path
                                    d="M24.5 22.5C27.9531 22.5 30.75 19.7031 30.75 16.25C30.75 12.7969 27.9531 10 24.5 10C21.0469 10 18.25 12.7969 18.25 16.25C18.25 19.7031 21.0469 22.5 24.5 22.5ZM24.5 25.625C20.3281 25.625 12 27.7188 12 31.875V33.4375C12 34.2969 12.7031 35 13.5625 35H35.4375C36.2969 35 37 34.2969 37 33.4375V31.875C37 27.7188 28.6719 25.625 24.5 25.625Z"
                                    fill="white"
                                    />
                                </svg>
                              </div>
                              <div className="card-text">
              <h4>{this.state.metrics.existingSubscribers}</h4>
                                <p>Existing subscribers</p>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="cards">
                              <div className="icon">
                                <svg
                                  width={50}
                                  height={50}
                                  viewBox="0 0 50 50"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                  >
                                  <circle cx={25} cy={25} r={25} fill="black" />
                                  <path
                                    d="M27.1865 34.4149C26.2369 35.1969 24.7749 35.1969 23.8253 34.4036L23.6879 34.2902C17.1279 28.9069 12.842 25.3822 13.0045 20.9848C13.0794 19.0581 14.1665 17.2108 15.9283 16.1228C19.2271 14.0827 23.3005 15.0347 25.4997 17.3694C27.6988 15.0347 31.7722 14.0714 35.071 16.1228C36.8328 17.2108 37.9199 19.0581 37.9949 20.9848C38.1698 25.3822 33.8714 28.9069 27.3115 34.3129L27.1865 34.4149Z"
                                    fill="white"
                                  />
                                </svg>
                              </div>
                              <div className="card-text">
                                <h4>{this.state.metrics.newSubscribers}</h4>
                                <p>New subscribers</p>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="cards">
                              <div className="icon">
                                <svg
                                  width={50}
                                  height={50}
                                  viewBox="0 0 50 50"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <circle cx={25} cy={25} r={25} fill="black" />
                                  <path
                                    d="M24.5 22.5C27.9531 22.5 30.75 19.7031 30.75 16.25C30.75 12.7969 27.9531 10 24.5 10C21.0469 10 18.25 12.7969 18.25 16.25C18.25 19.7031 21.0469 22.5 24.5 22.5ZM24.5 25.625C20.3281 25.625 12 27.7188 12 31.875V33.4375C12 34.2969 12.7031 35 13.5625 35H35.4375C36.2969 35 37 34.2969 37 33.4375V31.875C37 27.7188 28.6719 25.625 24.5 25.625Z"
                                    fill="white"
                                  />
                                </svg>
                              </div>
                              <div className="card-text">
                                <h4>{this.state.metrics.existingVendors}</h4>
                                <p>Existing vendors</p>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="chart-box ">
                              <div className="row">
                                <div className="col-lg-8">
                                  <div className="card-text">
                                    <h4>Purchase</h4>
                                    <svg
                                      width={205}
                                      height={54}
                                      viewBox="0 0 205 54"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path
                                        d="M203.889 48.5574L1.08358 49.1108L1.081 49.349L1.07841 49.5872L203.884 49.0338L203.887 48.7956L203.889 48.5574Z"
                                        stroke="#8D8D8D"
                                        strokeOpacity="0.15"
                                        strokeWidth="0.476388"
                                        strokeLinecap="square"
                                      />
                                      <path
                                        d="M203.984 30.9566L1.17831 31.51L1.17573 31.7482L1.17314 31.9864L203.979 31.433L203.981 31.1948L203.984 30.9566Z"
                                        stroke="#8D8D8D"
                                        strokeOpacity="0.15"
                                        strokeWidth="0.476388"
                                        strokeLinecap="square"
                                      />
                                      <path
                                        d="M204.079 13.3556L1.27328 13.909L1.2707 14.1472L1.26811 14.3854L204.074 13.832L204.076 13.5938L204.079 13.3556Z"
                                        stroke="#8D8D8D"
                                        strokeOpacity="0.15"
                                        strokeWidth="0.476388"
                                        strokeLinecap="square"
                                      />
                                      <path
                                        d="M1.06316 52.6782L22.8957 42.6047C24.2045 42.0008 25.7072 42.0145 26.9868 42.6419L43.2474 50.6147C45.4865 51.7125 48.2187 50.8736 49.4955 48.6962L72.964 8.67432C74.7829 5.57244 79.2149 5.45511 81.1065 8.45875L97.6303 34.6972C99.2086 37.2033 102.696 37.627 104.893 35.5794L115.806 25.4076C117.784 23.5643 120.87 23.6957 122.628 25.6981L131.165 35.4232C131.901 36.2613 132.913 36.8081 134.023 36.967L150.069 39.2647C150.675 39.3514 151.257 39.5547 151.783 39.8633L171.833 51.6326C172.533 52.0435 173.33 52.2659 174.146 52.2786L197.331 52.638"
                                        stroke="black"
                                        strokeLinejoin="round"
                                        />
                                    </svg>
                                  </div>
                                </div>
                                <div className="col-lg-4">
                                  <div className="card-text ct">
                                    <h4>20</h4>
                                    <h6>New purchase in last one month</h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-12">
                            <div className="cardss">
                              <div className="card-in">
                                <div className="work-title d-flex justify-content-between">
                                  <div className="worktop d-flex align-items-center">
                                    <h4>Work Order</h4>
                                    <small>23 December, Sunday</small>
                                  </div>
                                  <span>
                                    Show:
                                    <label className="custom">
                                      <select id>
                                        <option value={0}>This week</option>
                                        <option value={2}>last week</option>
                                        <option value={3}>last Month</option>
                                      </select>
                                    </label>
                                  </span>
                                </div>
                                <div className="level" />
                              </div>
                              <div className="card-in shad">
                                <div className="row">
                                  <div className="col-lg-6">
                                    <div className="work-inner">
                                      <h4>Internet installation</h4>
                                      <h6>
                                        Vendor: <span>Cave IT network</span>
                                      </h6>
                                      <p>
                                        Due date: <span>December 1, 2020</span>
                                      </p>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-inner-in">
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <div className="balls balls-orange" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="balls balls-green" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={22}
                                              height={22}
                                              viewBox="0 0 22 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              >
                                              <path
                                                d="M0 16.7544V21.1294H4.375L17.2783 8.22605L12.9033 3.85105L0 16.7544ZM20.6617 4.84272C21.1167 4.38772 21.1167 3.65272 20.6617 3.19772L17.9317 0.467715C17.4767 0.0127148 16.7417 0.0127148 16.2867 0.467715L14.1517 2.60272L18.5267 6.97772L20.6617 4.84272Z"
                                                fill="#D86018"
                                                />
                                            </svg>
                                          </div>
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={17}
                                              height={22}
                                              viewBox="0 0 17 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              >
                                              <path
                                                d="M1.16667 18.7961C1.16667 20.0794 2.21667 21.1294 3.5 21.1294H12.8333C14.1167 21.1294 15.1667 20.0794 15.1667 18.7961V4.79606H1.16667V18.7961ZM16.3333 1.29606H12.25L11.0833 0.129395H5.25L4.08333 1.29606H0V3.62939H16.3333V1.29606Z"
                                                fill="black"
                                              />
                                            </svg>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-button">
                                      <button className="work-btn">
                                        <a href="#">View Contact</a>
                                      </button>
                                      <button className="work-end-btn">
                                        <a href="#">Ended</a>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="card-in shad">
                                <div className="row">
                                  <div className="col-lg-6">
                                    <div className="work-inner">
                                      <h4>Internet installation</h4>
                                      <h6>
                                        Vendor: <span>Cave IT network</span>
                                      </h6>
                                      <p>
                                        Due date: <span>December 1, 2020</span>
                                      </p>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-inner-in">
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <div className="balls balls-orange" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="balls balls-green" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={22}
                                              height={22}
                                              viewBox="0 0 22 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              >
                                              <path
                                                d="M0 16.7544V21.1294H4.375L17.2783 8.22605L12.9033 3.85105L0 16.7544ZM20.6617 4.84272C21.1167 4.38772 21.1167 3.65272 20.6617 3.19772L17.9317 0.467715C17.4767 0.0127148 16.7417 0.0127148 16.2867 0.467715L14.1517 2.60272L18.5267 6.97772L20.6617 4.84272Z"
                                                fill="#D86018"
                                              />
                                            </svg>
                                          </div>
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={17}
                                              height={22}
                                              viewBox="0 0 17 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                            >
                                              <path
                                                d="M1.16667 18.7961C1.16667 20.0794 2.21667 21.1294 3.5 21.1294H12.8333C14.1167 21.1294 15.1667 20.0794 15.1667 18.7961V4.79606H1.16667V18.7961ZM16.3333 1.29606H12.25L11.0833 0.129395H5.25L4.08333 1.29606H0V3.62939H16.3333V1.29606Z"
                                                fill="black"
                                              />
                                            </svg>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-button">
                                      <button className="work-btn">
                                        <a href="#">View Contact</a>
                                      </button>
                                      <button className="work-end-btn">
                                        <a href="#">Ended</a>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="card-in shad">
                                <div className="row">
                                  <div className="col-lg-6">
                                    <div className="work-inner">
                                      <h4>Internet installation</h4>
                                      <h6>
                                        Vendor: <span>Cave IT network</span>
                                      </h6>
                                      <p>
                                        Due date: <span>December 1, 2020</span>
                                      </p>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-inner-in">
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <div className="balls balls-orange" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="balls balls-green" />
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={22}
                                              height={22}
                                              viewBox="0 0 22 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              >
                                              <path
                                                d="M0 16.7544V21.1294H4.375L17.2783 8.22605L12.9033 3.85105L0 16.7544ZM20.6617 4.84272C21.1167 4.38772 21.1167 3.65272 20.6617 3.19772L17.9317 0.467715C17.4767 0.0127148 16.7417 0.0127148 16.2867 0.467715L14.1517 2.60272L18.5267 6.97772L20.6617 4.84272Z"
                                                fill="#D86018"
                                                />
                                            </svg>
                                          </div>
                                        </div>
                                        <div className="col-lg-3">
                                          <div className="lev">
                                            <svg
                                              width={17}
                                              height={22}
                                              viewBox="0 0 17 22"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                            >
                                              <path
                                                d="M1.16667 18.7961C1.16667 20.0794 2.21667 21.1294 3.5 21.1294H12.8333C14.1167 21.1294 15.1667 20.0794 15.1667 18.7961V4.79606H1.16667V18.7961ZM16.3333 1.29606H12.25L11.0833 0.129395H5.25L4.08333 1.29606H0V3.62939H16.3333V1.29606Z"
                                                fill="black"
                                                />
                                            </svg>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-lg-3">
                                    <div className="work-button">
                                      <button className="work-btn">
                                        <a href="#">View Contact</a>
                                      </button>
                                      <button className="work-end-btn">
                                        <a href="#">Ended</a>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="card-in p-0">
                                <button className="cards-btn">
                                  <a href="#">See More</a>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-5">
                          <div className="plot-cards">
                            <h5 className="plot-cards-title">
                              Plot Allocation
                            </h5>
                            <div className="sub-chart d-flex align-items-center">
                              <p className="bl">
                                <label className="circle sm" />
                                Individual
                              </p>
                              <p className="bl">
                                {" "}
                                <label className="circle com sm" />
                                Commercial
                              </p>
                            </div>
                                <PlotAllocation/>
                          </div>
                        </div>
                        <div className="col-lg-7">
                          <div className="plot-cards">
                            <div className="work-title d-flex justify-content-between">
                              <h5 className="plot-cards-title">Plot Summary</h5>
                              <span>
                                Show:
                                <label className="custom">
                                  <select id>
                                    <option value={0}>This month</option>
                                    <option value={2}>last week</option>
                                    <option value={3}>last Month</option>
                                  </select>
                                </label>
                              </span>
                            </div>
                            <div className="divider mt-n2" />
                            <div className="row">
                              <div className="col-lg-8"></div>
                              <div className="col-lg-4">
                                <div className="chart-indicators">
                                  <div className="min-cicle">
                                    <span>Unavailable plot</span>
                                  </div>
                                  <div className="min-cicle">
                                    <span>Available plot</span>
                                  </div>
                                  <div className="min-cicle">
                                    <span>Developed environment</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="row">
                        <div className="col-lg-12">
                          <MessageBoard/>
                          </div>
                        <div className="col-lg-12">
                          <div className="cardss ">
                            <div className="week-task">
                              <h4 className="aquired mt-1">Aquired Plots</h4>
                            </div>
                            <div className="week-task d-flex justify-content-between">
                              <div className="sales">Closed-sales</div>
                              <label className="m-n2">
                                <span>
                                  Show:
                                  <label className="custom">
                                    <select id>
                                      <option value={0}>Monthly</option>
                                      <option value={2}>Yearly</option>
                                      <option value={3}>Weekly</option>
                                    </select>
                                  </label>
                                </span>
                                <span className="pl-2">
                                  Show:
                                  <label className="custom">
                                    <select id>
                                      <option value={0}>Online</option>
                                      <option value={2}>Offline</option>
                                    </select>
                                  </label>
                                </span>
                              </label>
                            </div>
                            <div className="divider mt-n2 mb-0" />
                            <div className="chart-inner pt-4">
                             <AquiredPlots/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </BodyTemplate>
      );
    }
  }
  
export default Index;
