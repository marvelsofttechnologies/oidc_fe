import React, { Component } from "react";
import "./App.css";
import ROUTES, { RenderRoutes } from "../../routes";
import { UserContext } from "../../contexts/UserContextt";
import { MainContext } from "../../contexts/MainContext";
import SideNav from "../Navigations/SideNav";
import { withRouter } from "react-router-dom";
import Alert from "../../utilities/Alert";
import TopNav from "../Navigations/TopNav";
import Template from "../../utilities/Template";

class App extends Component {
  state = {
    navOpen: false,
    setNav: this.setNav,
    loggedInUser: {},
    alert:{}
  };

  setNav = (nav) => {
    console.log(nav);
    this.setState({ navOpen: !this.state.navOpen });
  };

  updateUser = (user) => {
    this.setState({loggedInUser:user});
  }

  showAlert = (type,message,title) =>{
    let alert ={
      type:type,
      message:message,
      title:title,
      show:true
    }
    this.setState({alert:alert})
    setTimeout(() => {
      this.setState({alert:{}})
    }, 20000)
  }

  logout = () =>{
    localStorage.clear();
    let user = {};
    this.updateUser(user);
    window.location.href ='/login';
  }

  componentDidMount(){
    var user = JSON.parse(localStorage.getItem('user'));
    if(user != null){
      console.log(user);
      this.setState({loggedInUser:user});
    }else{
      if(this.props.location.pathname !== "/login" && this.props.location.pathname !== "/" ){
        window.location.href = "/"
      }
    }
  }
  render() {
    return (
      
        <MainContext.Provider value={{ ...this.state, setNav: this.setNav,updateUser:this.updateUser,showAlert:this.showAlert,logout:this.logout }}>
          <Template location={this.props.location} expanded={this.state.navOpen ? true : false} isLoginPath={!this.props.location.pathname.startsWith("/login") && this.props.location.pathname !== "/" ? true : false}>
            <RenderRoutes routes={ROUTES} />
          </Template>
        </MainContext.Provider>
      
    );
  }
}

export default withRouter(App);
