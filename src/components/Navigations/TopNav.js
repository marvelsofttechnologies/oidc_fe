import React, { Component } from "react";
import Notification from "../Notifications/Notification";
import { MainContext } from "../../contexts/MainContext";

class TopNav extends Component {
  static contextType = MainContext;
  state ={
    shownotes:false,
    notifications:{}
  }
  toggleNotes = () => {
    this.setState({shownotes:!this.state.shownotes})
  }

  logout = () => {
    const context = this.context;
    context.logout();
  }

  getUserNotifications = () =>{
    
  }
  render() {
    return (
      <>
      <Notification shoNotification={this.state.shownotes}/>
        <div className={this.props.expanded ?"top-nav d-flex fixed expand" : "top-nav d-flex fixed"}>
          <div className="search">
            <img src="/Assets/search.png" alt />
            <input type="search" name id="search" placeholder="Portal search" />
          </div>
          <div class="d-flex">
          <div className="bell" onClick={this.toggleNotes}>
            <img src="/Assets/bell.png" alt onClick={this.toggleNotes}/>
            <div className="circlee" />
          </div>
          <div className="logout" onClick={this.logout}>
            <img src="/Assets/powe.png" alt onClick={this.logout}/>
          </div>
          </div>
        </div>
      </>
    );
  }
}

export default TopNav;
