import React, { Component } from "react";
import SignIn from "../SignIn/SignIn";
import { Link } from "react-router-dom";

class NavBar extends Component {
  state = {
    signInActive: false,
  };

  toggleSignIn = () => {
    this.setState({ signInActive: !this.state.signInActive });
  };
  render() {
    return (
      <>
        <nav id="navbars">
          <div className="container">
            <div className="nav-logo">
              <Link to="">
                <img src="./Assets/Orange_Logos_White 1.png" alt="" />
              </Link>
            </div>
            <input type="checkbox" name id="menu" />
            <label htmlFor="menu" className="hamburger">
              <div className="burger" />
            </label>
            <ul>
              <li>
                <Link to="" className="list-items">
                  About
                </Link>
              </li>
              <li>
                <Link to="" className="list-items">
                  Gallery
                </Link>
              </li>
              <li>
                {this.props.in == "vendor" ? (
                  <Link to="/" className="list-items">
                    Subscriber
                  </Link>
                ) : (
                  <Link to="/vendor" className="list-items">
                    Vendor
                  </Link>
                )}
              </li>
              <li>
                <Link
                  to=""
                  className="list-items"
                  onClick={this.toggleSignIn}
                >
                  Log in
                </Link>
              </li>
            </ul>
            <Link
              to=""
              className="login-btn"
              onClick={this.toggleSignIn}
            >
              Log in
            </Link>
          </div>
        </nav>
        <SignIn active={this.state.signInActive} />
      </>
    );
  }
}

export default NavBar;
