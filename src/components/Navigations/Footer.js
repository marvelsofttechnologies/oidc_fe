import React from 'react';

function Footer(props) {
    return (
        <div className="footer d-flex justify-content-center">
        <ul>
          <li>
            <a href="#">Privacy policy</a>
          </li>
          <li>
            <a href="#">Documentation</a>
          </li>
          <li>
            <a href="#">Our website</a>
          </li>
        </ul>
      </div>
    );
}

export default Footer;