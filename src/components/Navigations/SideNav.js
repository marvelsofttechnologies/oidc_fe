import React, { Component } from "react";
import { MainContext } from "../../contexts/MainContext";
import TopNav from "./TopNav";
import { Link } from "react-router-dom";

class SideNav extends Component {
  static contextType = MainContext;
  state = {
    navOpen: false,
    user: {},
  };

  revealDropdownMenu = (event) => {
    console.log(event.target.closest("div"));
    event.target.classList.forEach(element => {
      console.log(element)
    });
    if(event.target.classList.contains("user-icon")){
      console.log(true);
    }else{
      console.log(false);
    }
  }

  toggleNav = () => {
    const context = this.context;
    console.log("nav toggled");
    console.log(this.context);
    this.setState({ navOpen: !this.state.navOpen });
    context.setNav(!this.state.navOpen);
  };
  getNavLinkClass = (path) => {
    return this.props.location.pathname.startsWith(path) ? "current" : "";
  };

  logout = () => {
    const context = this.context;
    context.logout();
  }
  
  componentDidMount() {
    console.log(this.context);
    let context = this.context;
    this.setState({ user: context.user, setUser: context.setUser });
  }

  render() {
    return (
      <MainContext.Consumer>
        {context => 
      <>
          
        <div
        // style={{position:"static"}}
          className={
            this.state.navOpen
              ? "sidenav pb-3 fixed expand"
              : "sidenav pb-3 fixed"
          }
        >
          <div className="logo" onClick={this.toggleNav}>
            <img src="/Assets/logo.png" alt onClick={this.toggleNav} />
          </div>
          <div className="divider" />
          <div className="side-list">
            <div className="user-icon align-item-center ml-3">
              <div className="user-image mr-3">
                <img src="/Assets/img.png" alt />
              </div>
              <div className="user-text">
        <Link href="#">{context.loggedInUser.fullname ? context.loggedInUser.fullname : "Adelowo Ajibola"}</Link>
                <p>{context.loggedInUser.email}</p>
              </div>
            </div>
            <ul className="side-content">
              <li className={`list-items dash ${this.getNavLinkClass("/app")}`}>
              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.5 8.13333C1.33431 8.13333 1.2 7.99902 1.2 7.83333V1.5C1.2 1.33431 1.33431 1.2 1.5 1.2H6.16667C6.33235 1.2 6.46667 1.33431 6.46667 1.5V7.83333C6.46667 7.99902 6.33235 8.13333 6.16667 8.13333H1.5ZM1.5 14.8C1.33431 14.8 1.2 14.6657 1.2 14.5V11.5C1.2 11.3343 1.33431 11.2 1.5 11.2H6.16667C6.33235 11.2 6.46667 11.3343 6.46667 11.5V14.5C6.46667 14.6657 6.33235 14.8 6.16667 14.8H1.5ZM9.83333 14.8C9.66765 14.8 9.53333 14.6657 9.53333 14.5V8.16667C9.53333 8.00098 9.66765 7.86667 9.83333 7.86667H14.5C14.6657 7.86667 14.8 8.00098 14.8 8.16667V14.5C14.8 14.6657 14.6657 14.8 14.5 14.8H9.83333ZM9.53333 1.5C9.53333 1.33431 9.66765 1.2 9.83333 1.2H14.5C14.6657 1.2 14.8 1.33431 14.8 1.5V4.5C14.8 4.66569 14.6657 4.8 14.5 4.8H9.83333C9.66765 4.8 9.53333 4.66569 9.53333 4.5V1.5Z" stroke="#000" stroke-width="1.4"/>
                        </svg>{" "}
                <Link to="/app">Dashboard</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/subscribers"
                )}`}
              >
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 8C10.21 8 12 6.21 12 4C12 1.79 10.21 0 8 0C5.79 0 4 1.79 4 4C4 6.21 5.79 8 8 8ZM8 10C5.33 10 0 11.34 0 14V15C0 15.55 0.45 16 1 16H15C15.55 16 16 15.55 16 15V14C16 11.34 10.67 10 8 10Z" fill="black" fill-opacity="0.7"/>
                    </svg>{" "}
                <Link to="/subscribers">Subscribers</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/plots"
                )}`}
              >
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.17 1H4C2.9 1 2 1.9 2 3V17C2 18.1 2.9 19 4 19H18C19.1 19 20 18.1 20 17V7.83C20 7.3 19.79 6.79 19.41 6.42L14.58 1.59C14.21 1.21 13.7 1 13.17 1ZM7 13H15C15.55 13 16 13.45 16 14C16 14.55 15.55 15 15 15H7C6.45 15 6 14.55 6 14C6 13.45 6.45 13 7 13ZM7 9H15C15.55 9 16 9.45 16 10C16 10.55 15.55 11 15 11H7C6.45 11 6 10.55 6 10C6 9.45 6.45 9 7 9ZM7 5H12C12.55 5 13 5.45 13 6C13 6.55 12.55 7 12 7H7C6.45 7 6 6.55 6 6C6 5.45 6.45 5 7 5Z" fill="black" fill-opacity="0.7"/>
                    </svg> {" "}
                <Link to="/plots">Plot Inventory</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/calender"
                )}`}
              >
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.3333 8.77504C13.0917 8.53337 12.6917 8.53337 12.45 8.77504L8.825 12.4L7.5 11.075C7.25833 10.8334 6.85833 10.8334 6.61667 11.075C6.375 11.3167 6.375 11.7167 6.61667 11.9584L8.23333 13.575C8.55833 13.9 9.08333 13.9 9.40833 13.575L13.325 9.65837C13.575 9.41671 13.575 9.01671 13.3333 8.77504ZM15.8333 2.50004H15V1.66671C15 1.20837 14.625 0.833374 14.1667 0.833374C13.7083 0.833374 13.3333 1.20837 13.3333 1.66671V2.50004H6.66667V1.66671C6.66667 1.20837 6.29167 0.833374 5.83333 0.833374C5.375 0.833374 5 1.20837 5 1.66671V2.50004H4.16667C3.24167 2.50004 2.50833 3.25004 2.50833 4.16671L2.5 15.8334C2.5 16.75 3.24167 17.5 4.16667 17.5H15.8333C16.75 17.5 17.5 16.75 17.5 15.8334V4.16671C17.5 3.25004 16.75 2.50004 15.8333 2.50004ZM15 15.8334H5C4.54167 15.8334 4.16667 15.4584 4.16667 15V6.66671H15.8333V15C15.8333 15.4584 15.4583 15.8334 15 15.8334Z" fill="black" fill-opacity="0.7"/>
                        </svg>{" "}
                <Link to="/calender">Calender</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass("/vendors")}`}
              >
                <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.12496 9.875C5.01121 9.875 0.791626 10.9358 0.791626 13.0417V13.8333C0.791626 14.2688 1.14788 14.625 1.58329 14.625H12.6666C13.102 14.625 13.4583 14.2688 13.4583 13.8333V13.0417C13.4583 10.9358 9.23871 9.875 7.12496 9.875Z" fill="black" fill-opacity="0.7"/>
                        <path d="M3.75242 5.12496H10.5053C10.7191 5.12496 10.8933 4.95079 10.8933 4.73704V4.72121C10.8933 4.50746 10.7191 4.33329 10.5053 4.33329H10.2916C10.2916 3.16163 9.65034 2.15621 8.70825 1.60204V2.35413C8.70825 2.57579 8.53409 2.74996 8.31242 2.74996C8.09075 2.74996 7.91659 2.57579 7.91659 2.35413V1.27746C7.66325 1.21413 7.402 1.16663 7.12492 1.16663C6.84784 1.16663 6.58659 1.21413 6.33325 1.27746V2.35413C6.33325 2.57579 6.15909 2.74996 5.93742 2.74996C5.71575 2.74996 5.54159 2.57579 5.54159 2.35413V1.60204C4.5995 2.15621 3.95825 3.16163 3.95825 4.33329H3.75242C3.53867 4.33329 3.3645 4.50746 3.3645 4.72121V4.74496C3.3645 4.95079 3.53867 5.12496 3.75242 5.12496Z" fill="black" fill-opacity="0.7"/>
                        <path d="M7.12493 8.29163C8.59742 8.29163 9.82451 7.27829 10.1808 5.91663H4.06909C4.42534 7.27829 5.65243 8.29163 7.12493 8.29163Z" fill="black" fill-opacity="0.7"/>
                        <path d="M17.4008 2.93208L18.1371 2.275L17.5433 1.24583L16.6012 1.55458C16.4904 1.4675 16.3637 1.39625 16.2292 1.34083L16.0312 0.375H14.8437L14.6458 1.34083C14.5112 1.39625 14.3846 1.4675 14.2658 1.55458L13.3317 1.24583L12.7379 2.275L13.4742 2.93208C13.4583 3.06667 13.4583 3.20917 13.4742 3.34375L12.7379 4.01667L13.3317 5.04583L14.2817 4.745C14.3846 4.82417 14.5033 4.8875 14.6221 4.94292L14.8437 5.91667H16.0312L16.245 4.95083C16.3717 4.89542 16.4825 4.83208 16.5933 4.75292L17.5354 5.05375L18.1292 4.02458L17.3929 3.35167C17.4167 3.20125 17.4087 3.06667 17.4008 2.93208ZM15.4375 4.13542C14.8912 4.13542 14.4479 3.69208 14.4479 3.14583C14.4479 2.59958 14.8912 2.15625 15.4375 2.15625C15.9837 2.15625 16.4271 2.59958 16.4271 3.14583C16.4271 3.69208 15.9837 4.13542 15.4375 4.13542Z" fill="black" fill-opacity="0.7"/>
                        <path d="M15.3583 6.54204L14.6854 6.76371C14.6063 6.70038 14.5192 6.65288 14.4242 6.61329L14.2817 5.91663H13.4346L13.2921 6.60538C13.1971 6.64496 13.1021 6.70038 13.0229 6.75579L12.3579 6.53413L11.9304 7.27038L12.4529 7.73746C12.445 7.84038 12.445 7.93538 12.4529 8.03038L11.9304 8.51329L12.3579 9.24954L13.0388 9.03579C13.1179 9.09121 13.1971 9.13871 13.2842 9.17829L13.4267 9.87496H14.2738L14.4242 9.18621C14.5113 9.14663 14.5983 9.09913 14.6775 9.04371L15.3504 9.25746L15.7779 8.52121L15.2554 8.03829C15.2633 7.93538 15.2633 7.84038 15.2554 7.74538L15.7779 7.27829L15.3583 6.54204ZM13.8542 8.60038C13.4663 8.60038 13.1496 8.28371 13.1496 7.89579C13.1496 7.50788 13.4663 7.19121 13.8542 7.19121C14.2421 7.19121 14.5588 7.50788 14.5588 7.89579C14.5588 8.28371 14.2421 8.60038 13.8542 8.60038Z" fill="black" fill-opacity="0.7"/>
                        </svg>  {" "}
                <Link to="/vendors">Vendor</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/payments"
                )}`}
              >
                <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18 0H2C0.89 0 0.00999999 0.89 0.00999999 2L0 14C0 15.11 0.89 16 2 16H18C19.11 16 20 15.11 20 14V2C20 0.89 19.11 0 18 0ZM17 14H3C2.45 14 2 13.55 2 13V8H18V13C18 13.55 17.55 14 17 14ZM18 4H2V2H18V4Z" fill="black" fill-opacity="0.7"/>
                        </svg>{" "}
                <Link to="/payments">Payment</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/cashflow"
                )}`}
              >
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0.666691V15.3334C0 15.4806 0.119401 15.6 0.266667 15.6H15.7333C15.8806 15.6 16 15.4806 16 15.3334V0.666691C16 0.519425 15.8806 0.400024 15.7333 0.400024H0.266667C0.119401 0.400024 0 0.519425 0 0.666691ZM0.533333 15.0667V12.6667H1.33333C2.21667 12.6676 2.93242 13.3834 2.93333 14.2667C2.93333 14.414 3.05273 14.5334 3.2 14.5334H12.8C12.9473 14.5334 13.0667 14.414 13.0667 14.2667C13.0676 13.3834 13.7833 12.6676 14.6667 12.6667H15.4667V15.0667H0.533333ZM2.74219 12.6667H13.2578C12.8672 13.0101 12.6155 13.4841 12.5499 14H3.45013C3.38451 13.4841 3.13281 13.0101 2.74219 12.6667ZM0.533333 12.1334V9.73336H1.33333C2.21667 9.73427 2.93242 10.45 2.93333 11.3334C2.93333 11.4806 3.05273 11.6 3.2 11.6H12.8C12.9473 11.6 13.0667 11.4806 13.0667 11.3334C13.0676 10.45 13.7833 9.73427 14.6667 9.73336H15.4667V12.1334H0.533333ZM2.74219 9.73336H13.2578C12.8672 10.0767 12.6155 10.5508 12.5499 11.0667H3.45013C3.38451 10.5508 3.13281 10.0767 2.74219 9.73336ZM0.533333 0.933358H15.4667V9.20002H0.533333V0.933358Z" fill="black" fill-opacity="0.7"/>
                        <path d="M1.33332 6.80001C2.21665 6.80092 2.93241 7.51667 2.93332 8.40001C2.93332 8.54727 3.05272 8.66668 3.19998 8.66668H12.8C12.9472 8.66668 13.0666 8.54727 13.0666 8.40001C13.0676 7.51667 13.7833 6.80092 14.6666 6.80001C14.8139 6.80001 14.9333 6.68061 14.9333 6.53334V3.60001C14.9333 3.45274 14.8139 3.33334 14.6666 3.33334C13.7833 3.33243 13.0676 2.61667 13.0666 1.73334C13.0666 1.58608 12.9472 1.46667 12.8 1.46667H3.19998C3.05272 1.46667 2.93332 1.58608 2.93332 1.73334C2.93241 2.61667 2.21665 3.33243 1.33332 3.33334C1.18605 3.33334 1.06665 3.45274 1.06665 3.60001V6.53334C1.06665 6.68061 1.18605 6.80001 1.33332 6.80001ZM1.59998 3.85014C2.566 3.7267 3.32668 2.96602 3.45011 2.00001H12.5499C12.6733 2.96602 13.434 3.7267 14.4 3.85014V6.28347C13.434 6.40665 12.6732 7.16733 12.5499 8.13334H3.45011C3.32681 7.16733 2.566 6.40665 1.59998 6.28347V3.85014Z" fill="black" fill-opacity="0.7"/>
                        <path d="M7.99997 7.06671C9.03096 7.06671 9.86663 6.23103 9.86663 5.20004C9.86663 4.16905 9.03096 3.33337 7.99997 3.33337C6.96898 3.33337 6.1333 4.16905 6.1333 5.20004C6.13447 6.23051 6.9695 7.06553 7.99997 7.06671ZM7.99997 3.86671C8.7363 3.86671 9.3333 4.46371 9.3333 5.20004C9.3333 5.93637 8.7363 6.53337 7.99997 6.53337C7.26364 6.53337 6.66663 5.93637 6.66663 5.20004C6.66755 4.46397 7.2639 3.86762 7.99997 3.86671Z" fill="black" fill-opacity="0.7"/>
                        <path d="M14.4 8.13331H13.8667V8.66664H14.6667C14.814 8.66664 14.9334 8.54724 14.9334 8.39998V7.59998H14.4V8.13331Z" fill="black" fill-opacity="0.7"/>
                        <path d="M14.4 2.53334H14.9334V1.73334C14.9334 1.58608 14.814 1.46667 14.6667 1.46667H13.8667V2.00001H14.4V2.53334Z" fill="black" fill-opacity="0.7"/>
                        <path d="M1.59998 2.00001H2.13332V1.46667H1.33332C1.18605 1.46667 1.06665 1.58608 1.06665 1.73334V2.53334H1.59998V2.00001Z" fill="black" fill-opacity="0.7"/>
                        <path d="M2.13332 8.66664V8.13331H1.59998V7.59998H1.06665V8.39998C1.06665 8.54724 1.18605 8.66664 1.33332 8.66664H2.13332Z" fill="black" fill-opacity="0.7"/>
                        <path d="M14.4 11.0667H13.8667V11.6H14.6667C14.814 11.6 14.9334 11.4806 14.9334 11.3333V10.5333H14.4V11.0667Z" fill="black" fill-opacity="0.7"/>
                        <path d="M1.59998 10.5333H1.06665V11.3333C1.06665 11.4806 1.18605 11.6 1.33332 11.6H2.13332V11.0667H1.59998V10.5333Z" fill="black" fill-opacity="0.7"/>
                        <path d="M14.4 14H13.8667V14.5333H14.6667C14.814 14.5333 14.9334 14.4139 14.9334 14.2667V13.4667H14.4V14Z" fill="black" fill-opacity="0.7"/>
                        <path d="M1.59998 13.4667H1.06665V14.2667C1.06665 14.4139 1.18605 14.5333 1.33332 14.5333H2.13332V14H1.59998V13.4667Z" fill="black" fill-opacity="0.7"/>
                        </svg>{" "}
                <Link to="/cashflow">Cashflow</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/requests"
                )}`}
              >
                <svg width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.6899 7.09746V0H2.32746V7.08771L0 8.49878V17.1387H16V8.49888L13.6899 7.09746ZM14.5054 8.76799L13.69 9.2316V8.27329L14.5054 8.76799ZM12.6846 1.0053V9.80308L8.00002 12.4663L3.33276 9.813V1.0053H12.6846ZM2.32746 9.24146L1.49484 8.76813L2.32746 8.26337V9.24146ZM1.0053 16.1334V9.64622L7.99998 13.6227L14.9947 9.64622V16.1334H1.0053Z" fill="black" fill-opacity="0.7"/>
                        <path d="M7.47059 4.18575C7.63405 4.03315 7.84788 3.95745 8.07293 3.9732C8.46691 4.00037 8.78452 4.31795 8.81169 4.71196C8.83847 5.1 8.58567 5.44998 8.21059 5.54414C7.79959 5.64735 7.51255 6.01187 7.51255 6.43064V7.64407H8.51784V6.50232C9.33116 6.26718 9.87339 5.49555 9.81458 4.64279C9.75305 3.75083 9.03403 3.0318 8.1421 2.97028C7.63583 2.9354 7.15403 3.10606 6.78461 3.45085C6.42033 3.79084 6.21143 4.27154 6.21143 4.76966H7.21672C7.21676 4.54591 7.30693 4.33856 7.47059 4.18575Z" fill="black" fill-opacity="0.7"/>
                        <path d="M7.48877 8.56323H8.54135V9.61578H7.48877V8.56323Z" fill="black" fill-opacity="0.7"/>
                        </svg>{" "}
                <Link to="/requests">Request</Link>
              </li>
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/communications"
                )}`}
              >
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.1667 1H0.833374V15.6667H10.9167V13.8333H2.66671V4.66667L10 9.25L17.3334 4.66667V9.25H19.1667V1ZM10 7.41667L2.66671 2.83333H17.3334L10 7.41667ZM16.4167 11.0833L20.0834 14.75L16.4167 18.4167V15.6667H12.75V13.8333H16.4167V11.0833Z" fill="black" fill-opacity="0.7"/>
                        </svg>{" "}
                <Link to="/communications">Communication</Link>
              </li>
              <li class="list-items">
                        <div class="ber d-flex justify-content-between">
                        <div class="d-flex w-100">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.1667 1H0.833374V15.6667H10.9167V13.8333H2.66671V4.66667L10 9.25L17.3334 4.66667V9.25H19.1667V1ZM10 7.41667L2.66671 2.83333H17.3334L10 7.41667ZM16.4167 11.0833L20.0834 14.75L16.4167 18.4167V15.6667H12.75V13.8333H16.4167V11.0833Z" fill="black" fill-opacity="0.7"/>
                            </svg>

                    
                            <a href="/communication.html" onClick={this.revealDropdownMenu}>Communication</a>
                        </div>
                    <div class="arr">
                        <img src="/Assets/arrowdn.png" alt="" onClick={this.revealDropdownMenu}/>
                    </div>
                        </div>
                        <div class="dropdowns ">
                            <ul class="ul-dp">
                                <li class="dp-dwn">
                                    <div class="drops">
                                        <span class="sub-menu">Adminstrative</span>
                                        <div class="arr">
                                            <img src="/Assets/arrowdn.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="dropdowns reveal">
                                        <ul class="ul-dp">
                                            <li class="dp-dwn"> <span class="sub-menu">Contact</span></li>
                                            <li class="dp-dwn"> <span class="sub-menu">Profile</span></li>
                                            <li class="dp-dwn"> <span class="sub-menu">Unblock users</span></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="dp-dwn"> <span class="sub-menu">Contact Details Update</span></li>
                                <li class="dp-dwn"> <span class="sub-menu">Profile</span></li>
                                <li class="dp-dwn"> <span class="sub-menu">Unblock users</span></li>
                            </ul>
                        </div>
                    </li>
              <div className="divider set" />
              <li
                className={`list-items ${this.getNavLinkClass(
                  "/app/settings"
                )}`}
              >
                <img src="/Assets/menu.png" alt className="img-circles" />{" "}
                <Link to="/settings">Settings</Link>
              </li>
            </ul>
          </div>
          
        </div>
        
      </>}

      </MainContext.Consumer>
    );
  }
}

export default SideNav;
