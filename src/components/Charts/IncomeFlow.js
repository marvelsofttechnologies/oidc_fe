import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';

class IncomeFlow extends Component {
    state ={
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            datasets: [
              {
                label: '2020',
                fill: false,
                lineTension: 0.5,
                backgroundColor: 'rgba(216,96,24)',
                borderColor: 'rgba(216,96,24)',
                borderCapStyle: 'round',
                borderDash: [],
                borderWidth:0,
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(216,96,24)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(216,96,24)',
                pointHoverBorderColor: 'rgba(216,96,24)',
                pointHoverBorderWidth: 2,
                pointRadius: 0,
                pointHitRadius: 50,
                data: [65, 59, 57, 63,64,45,65,76,45,50,55,54]
              }
            ]
          },
        options:{
            legend:{
                display:false,
                position:'left',
            }
        }
    }
    render() {
        return (
            <Line data={this.state.data} options={this.state.options} width={100}
            height={50}
            options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default IncomeFlow;