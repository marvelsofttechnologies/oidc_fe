import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';

const data = {
    datasets: [{
        data: [80, 15,5],
        backgroundColor:['rgba(216,96,24)','rgba(29,33,37)','rgb(235,175,139)'],
        borderWidth:[0.5]
    }],
    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Available',
        'UnAvailable',
        'Waitlist'
    ]
};

class AddPlot extends Component {
    render() {
        return (
            <Pie data={data} width={100}
           
           options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default AddPlot;