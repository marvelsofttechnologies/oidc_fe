import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';

class AquiredPlots extends Component {
    state ={
        data: {
            labels: ['January', 'February', 'March', 'April'],
            datasets: [
              {
                label: '',
                fill: true,
                lineTension: 0.5,
                backgroundColor: 'rgba(216,96,24,0.3)',
                borderColor: 'rgba(216,96,24)',
                borderCapStyle: 'round',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(216,96,24)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 3,
                pointHitRadius: 50,
                data: [65, 59, 80, 81]
              }
            ]
          },
        options:{
            legend:{
                display:false,
                position:'left',
            }
        }
    }
    render() {
        return (
            <Line data={this.state.data} options={this.state.options}/>
        );
    }
}

export default AquiredPlots;