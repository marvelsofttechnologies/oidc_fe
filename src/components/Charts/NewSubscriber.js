import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';

class NewSubscriber extends Component {
    state ={
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            datasets: [
              {
                label: 'Individual',
                fill: true,
                lineTension: 0.5,
                backgroundColor: 'rgba(29,33,37)',
                borderColor: '',
                borderCapStyle: 'round',
                borderDash: [],
                borderWidth:0,
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(216,96,24)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 0,
                pointHitRadius: 50,
                data: [65, 59, 80, 81,13,45,65,76,45,12,34,54]
              },              {
                label: 'Commercial',
                fill: true,
                lineTension: 0.5,
                backgroundColor: 'rgba(216,96,24)',
                borderColor: '',
                borderCapStyle: 'round',
                borderDash: [],
                borderWidth:0,
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(25,173,93,0.3)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(25,173,93,0.3)',
                pointHoverBorderColor: 'rgba(25,173,93,0.3)',
                pointHoverBorderWidth: 2,
                pointRadius: 0,
                pointHitRadius: 50,
                data: [5, 10, 40, 61,53, 15,30,67,35,22,34,64]
              }
            ]
          },
        options:{
            legend:{
                display:false,
                position:'left',
            }
        }
    }
    render() {
        return (
            <Bar data={this.state.data} options={this.state.options} width={100}
            height={50}
            options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default NewSubscriber;