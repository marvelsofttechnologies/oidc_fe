import React, { Component } from 'react';
import {HorizontalBar} from 'react-chartjs-2';

class PlotDevelopment extends Component {
    state ={
        data:{
            labels:['Plots Developed','Undergoing Development','Awaiting Development','Unsold'],
            datasets:[
                {
                    label:'',
                    barThickness: 6,
                    minBarLength: 5,
                    data:[450,470,500,400],
                    backgroundColor:'rgba(216,96,24)',
                }
            ]
        },
        options:{
            scales: {
              yAxes: [
                {
                  ticks: {
                    beginAtZero: true,
                  },
                },
              ],
            },
          }
    }
    render() {
        return (
            <HorizontalBar data={this.state.data} options={this.state.options} width={100}
            height={400}
            options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default PlotDevelopment;