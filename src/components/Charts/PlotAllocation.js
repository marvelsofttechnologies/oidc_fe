import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
const data = {
    datasets: [{
        data: [80, 20],
        backgroundColor:['rgba(29,33,37)','rgba(216,96,24)'],
        borderWidth:[0.1]
    }],
    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Residential',
        'Commercial'
    ]
};

class PlotAllocation extends Component {
    render() {
        return (
           <Doughnut data={data} width={100}
           height={50}
           options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default PlotAllocation;