import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';

class NumberOfPlots extends Component {
    state ={
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            datasets: [
              {
                label: '2019',
                fill: false,
                lineTension: 0.5,
                backgroundColor: 'rgba(216,96,24)',
                borderColor: 'rgba(216,96,24)',
                borderCapStyle: 'round',
                borderDash: [],
                borderWidth:1,
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(216,96,24)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 0,
                pointHitRadius: 50,
                data: [65, 59, 80, 81,13,45,65,76,45,12,34,54]
              },              {
                label: '2020',
                fill: false,
                lineTension: 0.5,
                backgroundColor: 'rgba(0,0,0)',
                borderColor: 'rgba(0,0,0)',
                borderCapStyle: 'round',
                borderDash: [],
                borderWidth:1,
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(216,96,24)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(216,96,24)',
                pointHoverBorderColor: 'rgba(216,96,24)',
                pointHoverBorderWidth: 2,
                pointRadius: 0,
                pointHitRadius: 50,
                data: [5, 10, 40, 61,53, 15,30,67,35,22,34,64]
              }
            ]
          },
        options:{
            legend:{
                display:false,
                position:'left',
            }
        }
    }
    render() {
        return (
            <Bar data={this.state.data} options={this.state.options} width={100}
            height={50}
            options={{ maintainAspectRatio: false }}/>
        );
    }
}

export default NumberOfPlots;