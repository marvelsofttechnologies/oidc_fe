import React, { Component } from 'react';
import Fetch from '../../utilities/Fetch';
import {Link} from 'react-router-dom';

class Notification extends Component {
  state={
    notifications:[]
  }

  componentDidMount(){
    this.loadNotifications();
  }

  loadNotifications = async () =>{
   try{
    let response = await  Fetch('notifications','Get');
   let data = await response.json();
   if(data.status){
     console.log(data);
     let reversedNotes = data.data.reverse();
     let notes= [];
     for(let i =0;i <= 3;i++){
      notes.push(reversedNotes[i]);
     }
     this.setState({notifications:notes})
   }
   }catch(error){
     console.log(error);
   }
  }

    render() {
        return (
            <>
  <div className={this.props.shoNotification ? "notify-box expand" : "notify-box"}>
  <div className="notify-box-in">
  {this.state.notifications.map((note,index)=> {
    return <div className="single-not">
      <div className="fil-boxx"> <div className="fil-box" /></div>
      <div className="filbox-in">
        <div className="iconn"><i className="fas fa-info-circle" /></div>
        
          <div className="mesg-fill">
          <h4>{note.title}</h4>
          <p>{note.description}</p>
        </div>
        
      </div>
    </div>
    })}
    </div>
  <Link to='/communications'><div className="add-sub-btn no-radius mt-0">View more</div></Link>
</div>

            </>
        );
    }
}

export default Notification;