import React, { Component } from "react";
import Spinner from "../../utilities/Spinner";
import Fetch from "../../utilities/Fetch";

class SignIn extends Component {
  state = {
    loading: false,
    email: "",
    password: "",
    errorMessage: "",
  };

  handleChanges = (e) => {
    const input = e.target;
    const name = input.name;
    const value = input.type === "checkbox" ? input.checked : input.value;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    this.setState({ loading: true });
    var data = {
      email: this.state.email,
      password: this.state.password,
      rememberMe: false,
    };
    Fetch("auth/login", "post", data)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.status) {
          localStorage.setItem("user", JSON.stringify(data.data));
          localStorage.setItem("token", JSON.stringify(data.data.token));
          localStorage["token"] = data.data.token;
        } else {
          this.setState({ errorMessage: data.message });
        }
        this.setState({ loading: false });
      })
      .catch((error) => {
        console.error(error);
      });
    e.preventDefault();
  };

  render() {
    return (
      <>
        {this.props.in == "vendor" ? (
          <div className="forms">
            <div id="sign-up-form" className="vend d-none">
              <h3 className="formtext m-0 pb-3">Sign Up</h3>
              <form className="vendform">
                <input
                  name="email"
                  type="email"
                  placeholder="E-mail"
                  className="input-field"
                  onChange={this.handleChanges}
                  required
                />
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  className="input-field"
                  onChange={this.handleChanges}
                  required
                />
                <input
                  name="confirmPassword"
                  type="password"
                  placeholder="Re-type Password"
                  className="input-field"
                  onChange={this.handleChanges}
                  required
                />
                <button className="sign-btns">Sign Up</button>
                <span>
                  <a href>
                    {" "}
                    Already have an account?{" "}
                    <span className="slink">Log in</span>
                  </a>
                </span>
              </form>
            </div>
            <div id="sign-in-form" className="vend d-block">
              <h3 className="formtext m-0">Login</h3>
              <form className="vendform" onSubmit={this.handleSubmit}>
                {this.state.errorMessage ? (
                  <div className="text-center mb-3">
                  <span className="text-danger">{this.state.errorMessage}</span>
                  </div>
                ) : null}
                <input
                  name="email"
                  type="email"
                  placeholder="E-mail"
                  className="input-field"
                  onChange={this.handleChanges}
                  required
                />
                <input
                  name="password"
                  type="password"
                  placeholder="Password"
                  className="input-field"
                  onChange={this.handleChanges}
                  required
                />
                <button className="sign-btns">
                  {this.state.loading ? <Spinner size={"small"} /> : "Log In"}
                </button>
                <span>
                  <a href> Forgotten Password?</a>
                </span>
              </form>
            </div>
          </div>
        ) : (
          <div id="sign-in-form" className={this.props.active ? "show" : ""}>
            <form onSubmit={this.handleSubmit}>
              <input
                name="email"
                type="email"
                placeholder="E-mail"
                className="input-field"
                onChange={this.handleChanges}
                required
              />
              <input
                name="password"
                type="password"
                placeholder="Password"
                className="input-field"
                onChange={this.handleChanges}
                required
              />
              <button className="sign-btns">
                {this.state.loading ? <Spinner size={"small"} loading={this.state.loading}/> : "Log In"}
              </button>
              <span>
                <a href> Forgotten Password?</a>
              </span>
            </form>
          </div>
        )}
      </>
    );
  }
}

export default SignIn;
